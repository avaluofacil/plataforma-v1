/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.TipoPregunta;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfaceTipoPregunta {
   public boolean registrar(TipoPregunta tipoPregunta) throws Exception;
   public List<TipoPregunta> verTodo()throws Exception;
   public boolean editar(TipoPregunta tipoPregunta)throws Exception;    
}
