/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.Ciudad;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfaceCiudad {
    public boolean registrar(Ciudad idCiudad) throws Exception;
    public List<Ciudad> verTodo()throws Exception;
    public List<Ciudad> verPorPais(int idPais);
    public Ciudad verPorCiudad(int idCiudad);
    public boolean editar(Ciudad idCiudad)throws Exception;    
}
