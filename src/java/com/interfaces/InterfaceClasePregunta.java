/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.ClasePregunta;
import java.util.List;

/**
 *
 * @author Juanda
 */
public interface InterfaceClasePregunta {
    public boolean registrar(ClasePregunta clasePregunta) throws Exception;
    public List<ClasePregunta> verTodo()throws Exception;
    public boolean editar(ClasePregunta clasePregunta)throws Exception;
}
