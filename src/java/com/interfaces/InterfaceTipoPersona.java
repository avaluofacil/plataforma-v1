/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.TipoPersona;
import java.util.List;

/**
 *
 * @author Juanda
 */
public interface InterfaceTipoPersona {
    public boolean registrar(TipoPersona tipoPersona) throws Exception;
    public List<TipoPersona> verTodo() throws Exception;
    public TipoPersona verPorTipoPersona (Integer idTioPersona)throws Exception;
    public boolean editar(TipoPersona tipoPersona)throws Exception;
    
}
