/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.TipoInmueble;
import java.util.List;


/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfaceTipoInmueble {
    public boolean registrar(TipoInmueble idInmueble) throws Exception;
    public List<TipoInmueble> verTodo()throws Exception;
    public TipoInmueble verPorTipoInmueble(int idTipoInmueble);
    public boolean editar(TipoInmueble idInmueble)throws Exception;    
}
