/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interfaces;

import com.entidad.Localidad;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfaceLocalidad {

    public boolean registrar(Localidad localidad) throws Exception;
    public List<Localidad> verTodo() throws Exception;
    public List<Localidad> verPorCiudad(int idCiudad);    
    public Localidad verPorLocalidad (int idLocalidad);
    public boolean editar(Localidad localidad) throws Exception;
}
