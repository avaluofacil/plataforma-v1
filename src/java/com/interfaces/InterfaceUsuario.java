/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.Usuario;
import java.util.List;

/**
 *
 * @author Juanda
 */
public interface InterfaceUsuario {
    public boolean registrar(Usuario email) throws Exception;
    public List<Usuario> verTodo()throws Exception;
    public Usuario verPorEmailDiferente(Integer idUsuario, String email)throws Exception;
    public Usuario verPorEmail (String email)throws Exception; 
    public boolean editar(Usuario usser)throws Exception;    
}
