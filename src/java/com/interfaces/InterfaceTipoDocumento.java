/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.TipoDocumento;
import java.util.List;

/**
 *
 * @author Juanda
 */
public interface InterfaceTipoDocumento {
    public boolean registrar(TipoDocumento tipoDocumento) throws Exception;
    public List<TipoDocumento> verTodo()throws Exception;
    public TipoDocumento verPorTipoDocumento (Integer idTipoDocumento)throws Exception;
    public boolean editar(TipoDocumento tipoDocumento)throws Exception;
}
