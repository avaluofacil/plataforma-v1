/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.Pregunta;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfacePregunta {
    public boolean registrar(Pregunta idPregunta) throws Exception;
    public List<Pregunta> verTodo()throws Exception;
    public List<Pregunta> verPorHabitacione(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble)throws Exception;
    public List<Pregunta> verPorBano(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble)throws Exception;
    public List<Pregunta> verPorGaraje(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble)throws Exception;
    public List<Pregunta> verPorOtra(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble)throws Exception;
    public List<Pregunta> verPorAdicional(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble)throws Exception;
    public List<Pregunta> verPorGeografica(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble)throws Exception;
    public boolean editar(Pregunta usser)throws Exception;  
    public List<Pregunta> verFiltro(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble)throws Exception;
    }
