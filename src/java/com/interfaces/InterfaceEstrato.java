/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interfaces;

import com.entidad.Estrato;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfaceEstrato {

    public boolean registrar(Estrato idEstrato) throws Exception;
    public List<Estrato> verTodo() throws Exception;
    public List<Estrato> verPorPais(int idLocalidad) throws Exception;
    public Estrato verPorEstrato(int idEstrato);
    public boolean editar(Estrato idEstrato) throws Exception;
}
