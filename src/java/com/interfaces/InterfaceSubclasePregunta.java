/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.SubclasePregunga;
import java.util.List;

/**
 *
 * @author Juanda
 */
public interface InterfaceSubclasePregunta {
    public boolean registrar(SubclasePregunga subclasePregunga) throws Exception;
    public List<SubclasePregunga> verTodo()throws Exception;
    public List<SubclasePregunga> verPorClasePregunta(int idClasePregunta)throws Exception;
    public boolean editar(SubclasePregunga subclasePregunga)throws Exception;
}
