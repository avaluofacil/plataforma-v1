/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.Pais;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfacePais {
    public boolean registrar(Pais email) throws Exception;
    public List<Pais> verTodo()throws Exception;
    public Pais verPorPais(int idPais);
    public boolean editar(Pais usser)throws Exception;    
}
