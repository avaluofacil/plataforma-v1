/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.Intercepto;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfaceIntercepto {
    public boolean registrar(Intercepto idIntercepto) throws Exception;
    public List<Intercepto> verTodo()throws Exception;
    public Intercepto verPorIntercepto(int idPais, int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble);
    public boolean editar(Intercepto idIntercepto)throws Exception;    
}