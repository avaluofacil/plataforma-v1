/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interfaces;

import com.entidad.Admininstrador;
import java.util.List;

/**
 *
 * @author AnchorPriceGroup
 */
public interface InterfaceAdmininstrador {
    public boolean registrar(Admininstrador email) throws Exception;
    public List<Admininstrador> verTodo()throws Exception;
    public Admininstrador verPorEmailDiferente(Integer idAdmininstrador, String email)throws Exception;
    public Admininstrador verPorEmail (String email)throws Exception; 
    public boolean editar(Admininstrador usser)throws Exception;    
}
