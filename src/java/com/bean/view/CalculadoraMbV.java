/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import com.dao.DaoCiudad;
import com.dao.DaoPais;
import com.entidad.Ciudad;
import com.entidad.Pais;
import com.interfaces.InterfaceCiudad;
import com.interfaces.InterfacePais;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author Anchor Price Group
 */
@ManagedBean
@ViewScoped
public class CalculadoraMbV implements Serializable {

    private String finalidad;
    private String estratoLista;
    private int metrosCuadrados;
    private int respuesta;
    private List<SelectItem> ListaCiudad;
    private Ciudad ciudad;
    private List<Ciudad> ciudades;
    private List<SelectItem> ListaPais;
    private Pais pais;
    private List<Pais> paises;

    public CalculadoraMbV() {
        finalidad = new String();
        estratoLista = new String();
        metrosCuadrados = 0;
        ListaCiudad = new ArrayList<SelectItem>();
        ListaPais = new ArrayList<SelectItem>();
        this.ciudad = new Ciudad();
        this.pais = new Pais();
    }

    public void cambiarPreguntaPais() {
        if (pais.getIdPais() == null && pais.getIdPais().equals("0")) {
            ListaCiudad = new ArrayList();
        } else {
            ListaCiudad = new ArrayList();
            try {
                InterfaceCiudad daoCiudad = new DaoCiudad();
                ciudades = daoCiudad.verPorPais(pais.getIdPais());
                for (Ciudad ciudade : ciudades) {
                    ciudad = ciudade;
                    this.ListaCiudad.add(new SelectItem(ciudad.getIdCiudad().toString(), ciudad.getNomCiudad()));
                }
                ciudad = new Ciudad();
            } catch (Exception ex) {
            }
        }
    }

    public void calcularRespuesta() {
        if (finalidad.equals("1")) {
            if (estratoLista.equals("1")) {
                respuesta = 35000 + (metrosCuadrados * 180);
            }
            if (estratoLista.equals("2")) {
                respuesta = 35000 + (metrosCuadrados * 200);
            }
            if (estratoLista.equals("3")) {
                respuesta = 35000 + (metrosCuadrados * 220);
            }
            if (estratoLista.equals("4")) {
                respuesta = 35000 + (metrosCuadrados * 450);
            }
            if (estratoLista.equals("5")) {
                respuesta = 35000 + (metrosCuadrados * 600);
            }
            if (estratoLista.equals("6")) {
                respuesta = 35000 + (metrosCuadrados * 700);
            }
        }
        if (finalidad.equals("2")) {
            if (estratoLista.equals("1")) {
                respuesta = 70000 + (metrosCuadrados * 180);
            }
            if (estratoLista.equals("2")) {
                respuesta = 75000 + (metrosCuadrados * 200);
            }
            if (estratoLista.equals("3")) {
                respuesta = 90000 + (metrosCuadrados * 220);
            }
            if (estratoLista.equals("4")) {
                respuesta = 110000 + (metrosCuadrados * 450);
            }
            if (estratoLista.equals("5")) {
                respuesta = 130000 + (metrosCuadrados * 600);
            }
            if (estratoLista.equals("6")) {
                respuesta = 160000 + (metrosCuadrados * 1400);
            }
        }
    }

    public int getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(int metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    public String getEstratoLista() {
        return estratoLista;
    }

    public void setEstratoLista(String estratoLista) {
        this.estratoLista = estratoLista;
    }

    public String getFinalidad() {
        return finalidad;
    }

    public void setFinalidad(String finalidad) {
        this.finalidad = finalidad;
    }

    public int getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(int respuesta) {
        this.respuesta = respuesta;
    }

    public List<SelectItem> getListaCiudad() {
        return ListaCiudad;
    }

    public void setListaCiudad(List<SelectItem> ListaCiudad) {
        this.ListaCiudad = ListaCiudad;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public List<SelectItem> getListaPais() {
        try {
            InterfacePais daoPais = new DaoPais();
            paises = daoPais.verTodo();
            for (Pais paise : paises) {
                pais = paise;
                this.ListaPais.add(new SelectItem(pais.getIdPais().toString(), pais.getNomPais()));
            }
        } catch (Exception ex) {
            System.out.println("Error getPais" + ex);
        }
        pais = new Pais();
        return ListaPais;
    }

    public void setListaPais(List<SelectItem> ListaPais) {
        this.ListaPais = ListaPais;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(List<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }
}