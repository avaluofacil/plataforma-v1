/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import clases.Encriptar;
import com.bean.session.MbSlogin;
import com.dao.DaoTipoDocumento;
import com.dao.DaoTipoPersona;
import com.dao.DaoUsuario;
import com.entidad.TipoDocumento;
import com.entidad.TipoPersona;
import com.entidad.Usuario;
import com.interfaces.InterfaceTipoDocumento;
import com.interfaces.InterfaceTipoPersona;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
/**
 *
 * @author Juanda
 */
@ManagedBean
@ViewScoped
public class UsuarioMBV implements Serializable {

    private Usuario usser;
    private List<Usuario> listaUsuario;
    private String repetirPass;
    private List<SelectItem> ListaTipoDocumento;
    private TipoDocumento tipoDocumento;
    private List<TipoDocumento> tipoDocumentos;
    private List<SelectItem> ListatipoPersona;
    private TipoPersona tipoPersona;
    private List<TipoPersona> tipoPersonas;
    @ManagedProperty("#{mbSlogin}")
    private MbSlogin mbSlogin;

    public UsuarioMBV() {
        ListatipoPersona = new ArrayList();
        ListaTipoDocumento = new ArrayList();
        this.tipoDocumento = new TipoDocumento();
        this.tipoPersona = new TipoPersona();
        this.usser = new Usuario(tipoDocumento, tipoPersona);
    }

    public void registrar() throws Exception {
        try {
            if (!this.usser.getContrasena().equals(this.repetirPass)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR: ", " Las contraseñas no son iguales"));
                return;
            }
            DaoUsuario daoUsuario = new DaoUsuario();

            if (daoUsuario.verPorEmail(this.usser.getEmail()) != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR:", " El correo electronico " + this.usser.getEmail() + " ya esta en uso"));
                return;
            }

            this.usser.setContrasena(Encriptar.sha512(this.usser.getContrasena()));
            daoUsuario.registrar(this.usser);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO: ", " El registro se realizo correctamente"));

            this.usser = new Usuario();
        } catch (Exception ex) {
        }
    }

    public List<Usuario> getListarUsuario() {
        try {
            DaoUsuario daoUsuario = new DaoUsuario();
            this.listaUsuario = daoUsuario.verTodo();
            return this.listaUsuario;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public Usuario getPorEmail() {
        try {
            DaoUsuario daoUsuario = new DaoUsuario();
            HttpSession sessionUsuario = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            this.usser = daoUsuario.verPorEmail(sessionUsuario.getAttribute("email").toString());
            return this.usser;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public void actualizar() throws Exception {
        try {
            DaoUsuario daoUsuario = new DaoUsuario();
            if (daoUsuario.verPorEmailDiferente(this.usser.getId(), this.usser.getEmail()) != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR:", " El email " + this.usser.getEmail() + " ya esta ocupado"));
                return;
            }

            daoUsuario.editar(this.usser);

            this.mbSlogin.setEmail(this.usser.getEmail());

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO:", " Los datos fueron cambiados exitosamente"));

            HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            httpSession.setAttribute("email", this.usser.getEmail());
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public Usuario getUsser() {
        return usser;
    }

    public void setUsser(Usuario usser) {
        this.usser = usser;
    }

    public List<Usuario> getListaUsuario() {
        return listaUsuario;
    }

    public void setListaUsuario(List<Usuario> listaUsuario) {
        this.listaUsuario = listaUsuario;
    }

    public String getRepetirPass() {
        return repetirPass;
    }

    public void setRepetirPass(String repetirPass) {
        this.repetirPass = repetirPass;
    }

    public List<SelectItem> getListaTipoDocumento() {
        ListaTipoDocumento = new ArrayList();
        try {
            InterfaceTipoDocumento daoTipoDocumento = new DaoTipoDocumento();
            tipoDocumentos = daoTipoDocumento.verTodo();
            for (TipoDocumento tipoDocumento1 : tipoDocumentos) {
                tipoDocumento = tipoDocumento1;
                this.ListaTipoDocumento.add(new SelectItem(tipoDocumento.getId().toString(), tipoDocumento.getNombreTipo()));
            }
        } catch (Exception ex) {

        }
        return ListaTipoDocumento;
    }

    public void setListaTipoDocumento(List<SelectItem> ListaTipoDocumento) {
        this.ListaTipoDocumento = ListaTipoDocumento;
    }

    public List<SelectItem> getListatipoPersona() {
        ListatipoPersona = new ArrayList();
        try {
            InterfaceTipoPersona daoTipoPersona = new DaoTipoPersona();
            tipoPersonas = daoTipoPersona.verTodo();
            for (TipoPersona tipoPersona1 : tipoPersonas) {
                this.tipoPersona = tipoPersona1;
                this.ListatipoPersona.add(new SelectItem(tipoPersona.getId().toString(), tipoPersona.getNombreTipo()));
            }
        } catch (Exception ex) {
        }
        return ListatipoPersona;
    }

    public void setListatipoPersona(List<SelectItem> ListatipoPersona) {
        this.ListatipoPersona = ListatipoPersona;
    }

    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersona tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public MbSlogin getMbSlogin() {
        return mbSlogin;
    }

    public void setMbSlogin(MbSlogin mbSlogin) {
        this.mbSlogin = mbSlogin;
    }

    public List<TipoDocumento> getTipoDocumentos() {
        return tipoDocumentos;
    }

    public void setTipoDocumentos(List<TipoDocumento> tipoDocumentos) {
        this.tipoDocumentos = tipoDocumentos;
    }

    public List<TipoPersona> getTipoPersonas() {
        return tipoPersonas;
    }

    public void setTipoPersonas(List<TipoPersona> tipoPersonas) {
        this.tipoPersonas = tipoPersonas;
    }
}
