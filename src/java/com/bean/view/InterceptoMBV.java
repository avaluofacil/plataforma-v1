/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import com.dao.DaoCiudad;
import com.dao.DaoEstrato;
import com.dao.DaoIntercepto;
import com.dao.DaoLocalidad;
import com.dao.DaoPais;
import com.dao.DaoTipoInmueble;
import com.entidad.Ciudad;
import com.entidad.Estrato;
import com.entidad.Intercepto;
import com.entidad.Localidad;
import com.entidad.Pais;
import com.entidad.TipoInmueble;
import com.interfaces.InterfaceCiudad;
import com.interfaces.InterfaceEstrato;
import com.interfaces.InterfaceLocalidad;
import com.interfaces.InterfacePais;
import com.interfaces.InterfaceTipoInmueble;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Juanda
 */
@ManagedBean
@ViewScoped
public class InterceptoMBV implements Serializable {

    private Intercepto intercepto;
    private List<Intercepto> listaIntercepto;
    private List<SelectItem> ListaPais;
    private Pais pais;
    private List<Pais> paises;
    private List<SelectItem> ListaCiudad;
    private Ciudad ciudad;
    private List<Ciudad> ciudades;
    private List<SelectItem> ListaLocalidad;
    private Localidad localidad;
    private List<Localidad> localidades;
    private List<SelectItem> ListaEstrato;
    private Estrato estrato;
    private List<Estrato> estratos;
    private List<SelectItem> ListaTipoInmueble;
    private TipoInmueble tipoInmueble;
    private List<TipoInmueble> tipoInmuebles;

    public InterceptoMBV() {
        ListaPais = new ArrayList<SelectItem>();
        ListaCiudad = new ArrayList<SelectItem>();
        ListaLocalidad = new ArrayList<SelectItem>();
        ListaEstrato = new ArrayList<SelectItem>();
        ListaTipoInmueble = new ArrayList<SelectItem>();
        this.pais = new Pais();
        this.ciudad = new Ciudad();
        this.localidad = new Localidad();
        this.estrato = new Estrato();
        this.tipoInmueble = new TipoInmueble();
        this.intercepto = new Intercepto(ciudad, estrato, localidad, pais, tipoInmueble);
    }

    public void cambiarPreguntaLocalidad() {
        if (intercepto.getLocalidad().getIdLocalidad() == null && intercepto.getLocalidad().getIdLocalidad().equals("0")) {
            ListaEstrato = new ArrayList();
        } else {
            ListaEstrato = new ArrayList();
            try {
                InterfaceEstrato daoEstrato = new DaoEstrato();
                estratos = daoEstrato.verPorPais(intercepto.getLocalidad().getIdLocalidad());
                for (Estrato estrato1 : estratos) {
                    estrato = estrato1;
                    this.ListaEstrato.add(new SelectItem(estrato.getIdEstrato().toString(), estrato.getNomEstrato()));
                }
            } catch (Exception ex) {
            }
        }
    }

    public void cambiarPreguntaCiudad() {
        if (intercepto.getCiudad().getIdCiudad() == null && intercepto.getCiudad().getIdCiudad().equals("0")) {
            ListaLocalidad = new ArrayList();
        } else {
            ListaLocalidad = new ArrayList();
            try {
                InterfaceLocalidad daoLocalidad = new DaoLocalidad();
                localidades = daoLocalidad.verPorCiudad(intercepto.getCiudad().getIdCiudad());
                for (Localidad localidade : localidades) {
                    localidad = localidade;
                    this.ListaLocalidad.add(new SelectItem(localidad.getIdLocalidad().toString(), localidad.getNomLocalidad()));
                }
            } catch (Exception ex) {
            }
        }
    }

    public void cambiarPreguntaPais() {
        if (intercepto.getPais().getIdPais() == null && intercepto.getPais().getIdPais().equals("0")) {
            ListaCiudad = new ArrayList();
        } else {
            ListaCiudad = new ArrayList();
            try {
                InterfaceCiudad daoCiudad = new DaoCiudad();
                ciudades = daoCiudad.verPorPais(intercepto.getPais().getIdPais());
                for (Ciudad ciudade : ciudades) {
                    ciudad = ciudade;
                    this.ListaCiudad.add(new SelectItem(ciudad.getIdCiudad().toString(), ciudad.getNomCiudad()));
                }
            } catch (Exception ex) {

            }
        }
    }

    public void registrar() throws Exception {
        try {
            DaoIntercepto daoIntercepto = new DaoIntercepto();
            daoIntercepto.registrar(this.intercepto);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO: ", " El registro se realizo correctamente"));
            this.intercepto = new Intercepto();
        } catch (Exception ex) {
        }
    }

    public List<Intercepto> getListarIntercepto() {
        try {
            DaoIntercepto daoIntercepto = new DaoIntercepto();
            this.listaIntercepto = daoIntercepto.verTodo();
            return this.listaIntercepto;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public void actualizar() throws Exception {
        try {
            DaoIntercepto daoIntercepto = new DaoIntercepto();
            daoIntercepto.editar(this.intercepto);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO:", " Los datos fueron cambiados exitosamente"));

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public List<SelectItem> getListaPais() {
        try {
            ListaPais = new ArrayList();
            InterfacePais daoPais = new DaoPais();
            paises = daoPais.verTodo();
            for (Pais paise : paises) {
                pais = paise;
                this.ListaPais.add(new SelectItem(pais.getIdPais().toString(), pais.getNomPais()));
            }
        } catch (Exception ex) {
        }
        return ListaPais;
    }

    public void setListaPais(List<SelectItem> ListaPais) {
        this.ListaPais = ListaPais;
    }

    public Intercepto getIntercepto() {
        return intercepto;
    }

    public void setIntercepto(Intercepto intercepto) {
        this.intercepto = intercepto;
    }

    public List<Intercepto> getListaIntercepto() {
        return listaIntercepto;
    }

    public void setListaIntercepto(List<Intercepto> listaIntercepto) {
        this.listaIntercepto = listaIntercepto;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }

    public List<SelectItem> getListaCiudad() {
        return ListaCiudad;
    }

    public void setListaCiudad(List<SelectItem> ListaCiudad) {
        this.ListaCiudad = ListaCiudad;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(List<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public List<SelectItem> getListaLocalidad() {
        return ListaLocalidad;
    }

    public void setListaLocalidad(List<SelectItem> ListaLocalidad) {
        this.ListaLocalidad = ListaLocalidad;
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public List<SelectItem> getListaEstrato() {
        return ListaEstrato;
    }

    public void setListaEstrato(List<SelectItem> ListaEstrato) {
        this.ListaEstrato = ListaEstrato;
    }

    public Estrato getEstrato() {
        return estrato;
    }

    public void setEstrato(Estrato estrato) {
        this.estrato = estrato;
    }

    public List<Estrato> getEstratos() {
        return estratos;
    }

    public void setEstratos(List<Estrato> estratos) {
        this.estratos = estratos;
    }

    public List<SelectItem> getListaTipoInmueble() {
        try {
            ListaTipoInmueble = new ArrayList();
            InterfaceTipoInmueble daoTipoInmueble = new DaoTipoInmueble();
            tipoInmuebles = daoTipoInmueble.verTodo();
            for (TipoInmueble tipoInmueble1 : tipoInmuebles) {
                tipoInmueble = tipoInmueble1;
                this.ListaTipoInmueble.add(new SelectItem(tipoInmueble.getIdTipoInmueble().toString(), tipoInmueble.getNomTipoInmueble()));
            }
        } catch (Exception ex) {
        }
        return ListaTipoInmueble;
    }

    public void setListaTipoInmueble(List<SelectItem> ListaTipoInmueble) {
        this.ListaTipoInmueble = ListaTipoInmueble;
    }

    public TipoInmueble getTipoInmueble() {
        return tipoInmueble;
    }

    public void setTipoInmueble(TipoInmueble tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    public List<TipoInmueble> getTipoInmuebles() {
        return tipoInmuebles;
    }

    public void setTipoInmuebles(List<TipoInmueble> tipoInmuebles) {
        this.tipoInmuebles = tipoInmuebles;
    }
}
