/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import com.dao.DaoCiudad;
import com.dao.DaoPais;
import com.entidad.Ciudad;
import com.entidad.Pais;
import com.interfaces.InterfacePais;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
@ManagedBean
@ViewScoped
public class CiudadMBV implements Serializable {

    private Ciudad ciudad;
    private List<Ciudad> listaCiudad;
    private List<SelectItem> ListaPais;
    private Pais pais;
    private List<Pais> paises;

    public CiudadMBV() {
        ListaPais = new ArrayList<SelectItem>();
        this.pais = new Pais();
        this.ciudad = new Ciudad(pais);
    }

    public void registrar() throws Exception {

        try {
            DaoCiudad daoCiudad = new DaoCiudad();
            daoCiudad.registrar(this.ciudad);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO: ", " El registro se realizo correctamente"));
            this.ciudad = new Ciudad();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public List<Ciudad> getListarCiudad() {
        try {
            DaoCiudad daoCiudad = new DaoCiudad();
            this.listaCiudad = daoCiudad.verTodo();
            return this.listaCiudad;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public void actualizar() throws Exception {
        try {
            DaoCiudad daoCiudad = new DaoCiudad();
            daoCiudad.editar(this.ciudad);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO:", " Los datos fueron cambiados exitosamente"));

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public List<Ciudad> getListaCiudad() {
        return listaCiudad;
    }

    public void setListaCiudad(List<Ciudad> listaCiudad) {
        this.listaCiudad = listaCiudad;
    }

    public List<SelectItem> getListaPais() {
        try {
            ListaPais = new ArrayList<SelectItem>();
            InterfacePais daoPais = new DaoPais();
            paises = daoPais.verTodo();
            for (Pais paise : paises) {
                pais = paise;
                this.ListaPais.add(new SelectItem(pais.getIdPais().toString(), pais.getNomPais()));
            }
            this.pais = new Pais();
        } catch (Exception ex) {
        }
        return ListaPais;
    }

    public void setListaPais(List<SelectItem> ListaPais) {
        this.ListaPais = ListaPais;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }
}
