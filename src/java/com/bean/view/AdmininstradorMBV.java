/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import clases.Encriptar;
import com.bean.session.MbSlogin;
import com.dao.DaoAdmininstrador;
import com.dao.DaoTipoDocumento;
import com.entidad.Admininstrador;
import com.entidad.TipoDocumento;
import com.interfaces.InterfaceTipoDocumento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Juanda
 */
@ManagedBean
@ViewScoped
public class AdmininstradorMBV implements Serializable {

    private Admininstrador admininstrador;
    private List<Admininstrador> listaAdmininstrador;
    private String repetirPass;
    private List<SelectItem> listaTipoDocumento;
    private TipoDocumento tipoDocumento;
    private List<TipoDocumento> tipoDocumentos;
    @ManagedProperty("#{mbSlogin}")
    private MbSlogin mbSlogin;

    public AdmininstradorMBV() {
        this.admininstrador = new Admininstrador();
        listaTipoDocumento = new ArrayList();
        this.tipoDocumento = new TipoDocumento();
    }

    public void registrar() throws Exception {
        try {
            if (!this.admininstrador.getContrasena().equals(this.repetirPass)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR: ", " Las contraseñas no son iguales"));
                return;
            }
            DaoAdmininstrador daoAdmininstrador = new DaoAdmininstrador();

            if (daoAdmininstrador.verPorEmail(this.admininstrador.getEmail()) != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR:", " El correo electronico " + this.admininstrador.getEmail() + " ya esta en uso"));
                return;
            }

            this.admininstrador.setContrasena(Encriptar.sha512(this.admininstrador.getContrasena()));
            daoAdmininstrador.registrar(this.admininstrador);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO: ", " El registro se realizo correctamente"));

            this.admininstrador = new Admininstrador();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public List<Admininstrador> getListarAdmininstador() {
        try {
            DaoAdmininstrador daoAdmininstrador = new DaoAdmininstrador();
            this.listaAdmininstrador = daoAdmininstrador.verTodo();
            return this.listaAdmininstrador;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public Admininstrador getPorEmail() {
        try {
            DaoAdmininstrador daoAdmininstrador = new DaoAdmininstrador();
            HttpSession sessionUsuario = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            this.admininstrador = daoAdmininstrador.verPorEmail(sessionUsuario.getAttribute("email").toString());
            return this.admininstrador;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public void actualizar() throws Exception {
        try {
            DaoAdmininstrador daoAdmininstrador = new DaoAdmininstrador();
            if (daoAdmininstrador.verPorEmailDiferente(this.admininstrador.getIdAdmininstrador(), this.admininstrador.getEmail()) != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR:", " El email " + this.admininstrador.getEmail() + " ya esta ocupado"));
                return;
            }
            daoAdmininstrador.editar(this.admininstrador);
            this.mbSlogin.setEmail(this.admininstrador.getEmail());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO:", " Los datos fueron cambiados exitosamente"));
            HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            httpSession.setAttribute("email", this.admininstrador.getEmail());
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public Admininstrador getAdmininstrador() {
        return admininstrador;
    }

    public void setAdmininstrador(Admininstrador admininstrador) {
        this.admininstrador = admininstrador;
    }

    public List<Admininstrador> getListaAdmininstrador() {
        return listaAdmininstrador;
    }

    public void setListaAdmininstrador(List<Admininstrador> listaAdmininstrador) {
        this.listaAdmininstrador = listaAdmininstrador;
    }

    public String getRepetirPass() {
        return repetirPass;
    }

    public void setRepetirPass(String repetirPass) {
        this.repetirPass = repetirPass;
    }

    public List<SelectItem> getListaTipoDocumento() {
        try {
            InterfaceTipoDocumento daoTipoDocumento = new DaoTipoDocumento();
            tipoDocumentos = daoTipoDocumento.verTodo();
            for (TipoDocumento tipoDocumento1 : tipoDocumentos) {
                tipoDocumento = tipoDocumento1;
                this.listaTipoDocumento.add(new SelectItem(tipoDocumento.getId().toString(), tipoDocumento.getNombreTipo()));
            }
        } catch (Exception ex) {
        }
        return listaTipoDocumento;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public MbSlogin getMbSlogin() {
        return mbSlogin;
    }

    public void setMbSlogin(MbSlogin mbSlogin) {
        this.mbSlogin = mbSlogin;
    }

    public List<TipoDocumento> getTipoDocumentos() {
        return tipoDocumentos;
    }

    public void setTipoDocumentos(List<TipoDocumento> tipoDocumentos) {
        this.tipoDocumentos = tipoDocumentos;
    }
}
