/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import com.bean.session.MbSfiltroPreguntas;
import com.dao.DaoCiudad;
import com.dao.DaoClasePregunta;
import com.dao.DaoEstrato;
import com.dao.DaoLocalidad;
import com.dao.DaoPais;
import com.dao.DaoPregunta;
import com.dao.DaoSubclasePregunta;
import com.dao.DaoTipoInmueble;
import com.dao.DaoTipoPregunta;
import com.entidad.Ciudad;
import com.entidad.ClasePregunta;
import com.entidad.Estrato;
import com.entidad.Localidad;
import com.entidad.Pais;
import com.entidad.Pregunta;
import com.entidad.SubclasePregunga;
import com.entidad.TipoInmueble;
import com.entidad.TipoPregunta;
import com.interfaces.InterfaceCiudad;
import com.interfaces.InterfaceClasePregunta;
import com.interfaces.InterfaceEstrato;
import com.interfaces.InterfaceLocalidad;
import com.interfaces.InterfacePais;
import com.interfaces.InterfaceSubclasePregunta;
import com.interfaces.InterfaceTipoInmueble;
import com.interfaces.InterfaceTipoPregunta;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Juanda
 */
@ManagedBean
@ViewScoped

public class PreguntaMBV implements Serializable {

    private Pregunta pregunta;
    private List<Pregunta> listaPregunta;
    private List<SelectItem> ListaTipoPregunta;
    private TipoPregunta tipoPregunta;
    private List<TipoPregunta> tipoPreguntas;
    private List<SelectItem> ListaPais;
    private Pais pais;
    private List<Pais> paises;
    private List<SelectItem> ListaTipoInmueble;
    private TipoInmueble tipoInmueble;
    private List<TipoInmueble> tipoInmuebles;
    private List<SelectItem> ListaCiudad;
    private Ciudad ciudad;
    private List<Ciudad> ciudades;
    private List<SelectItem> ListaLocalidad;
    private Localidad localidad;
    private List<Localidad> localidades;
    private List<SelectItem> ListaEstrato;
    private Estrato estrato;
    private List<Estrato> estratos;
    private ClasePregunta clasePregunta;
    private List<SelectItem> ListaClasePregunta;
    private List<ClasePregunta> clasePreguntas;
    private SubclasePregunga subclasePregunga;
    private List<SelectItem> listaSubClasePregunta;
    private List<SubclasePregunga> subclasePregungas;
    @ManagedProperty("#{mbSfiltroPreguntas}")
    private MbSfiltroPreguntas filtro;
            
            
    public PreguntaMBV() {
        ListaEstrato = new ArrayList();
        ListaLocalidad = new ArrayList();
        ListaTipoInmueble = new ArrayList();
        ListaTipoPregunta = new ArrayList();
        ListaCiudad = new ArrayList();
        ListaPais = new ArrayList();
        ListaClasePregunta = new ArrayList();
        listaSubClasePregunta = new ArrayList();
        this.tipoInmueble = new TipoInmueble();
        this.tipoPregunta = new TipoPregunta();
        this.ciudad = new Ciudad();
        this.pais = new Pais();
        this.localidad = new Localidad();
        this.estrato = new Estrato();
        this.clasePregunta = new ClasePregunta();
        this.subclasePregunga = new SubclasePregunga();
        this.pregunta = new Pregunta(ciudad, clasePregunta, estrato, localidad, subclasePregunga, tipoInmueble, tipoPregunta);
    }

    public void cambiarPreguntaLocalidad() {
        if (pregunta.getLocalidad().getIdLocalidad() == null && pregunta.getLocalidad().getIdLocalidad().equals("0")) {
            ListaEstrato = new ArrayList();
        } else {
            ListaEstrato = new ArrayList();
            try {
                InterfaceEstrato daoEstrato = new DaoEstrato();
                estratos = daoEstrato.verPorPais(pregunta.getLocalidad().getIdLocalidad());
                for (Estrato estrato1 : estratos) {
                    estrato = estrato1;
                    this.ListaEstrato.add(new SelectItem(estrato.getIdEstrato().toString(), estrato.getNomEstrato()));
                }
            } catch (Exception ex) {
            }
        }
    }
    
    public void cambiarPreguntaCiudad() {
        if (pregunta.getCiudad().getIdCiudad() == null && pregunta.getCiudad().getIdCiudad().equals("0")) {
            ListaLocalidad = new ArrayList();
        } else {
            ListaLocalidad = new ArrayList();
            try {
                InterfaceLocalidad daoLocalidad = new DaoLocalidad();
                localidades = daoLocalidad.verPorCiudad(pregunta.getCiudad().getIdCiudad());
                for (Localidad localidade : localidades) {
                    localidad = localidade;
                    this.ListaLocalidad.add(new SelectItem(localidad.getIdLocalidad().toString(), localidad.getNomLocalidad()));
                }
            } catch (Exception ex) {

            }
        }
    }
    
    public void cambiarPreguntaPais() {
        if (pais.getIdPais() == null && pais.getIdPais().equals("0")) {
            ListaCiudad = new ArrayList();
        } else {
            ListaCiudad = new ArrayList();
            try {
                InterfaceCiudad daoCiudad = new DaoCiudad();
                ciudades = daoCiudad.verPorPais(pais.getIdPais());
                for (Ciudad ciudade : ciudades) {
                    ciudad = ciudade;
                    this.ListaCiudad.add(new SelectItem(ciudad.getIdCiudad().toString(), ciudad.getNomCiudad()));
                }
            } catch (Exception ex) {
            }

        }
    }

    public void cambiarPregunta() {
        if (pregunta.getClasePregunta().getIdClasePregunta() == null && pregunta.getClasePregunta().getIdClasePregunta().equals("0")) {
            listaSubClasePregunta = new ArrayList();
        } else {
            listaSubClasePregunta = new ArrayList();
            try {
                InterfaceSubclasePregunta daosubClasePregunta = new DaoSubclasePregunta();
                subclasePregungas = daosubClasePregunta.verPorClasePregunta(pregunta.getClasePregunta().getIdClasePregunta());
                for (SubclasePregunga subclasePregunga1 : subclasePregungas) {
                    subclasePregunga = subclasePregunga1;
                    this.listaSubClasePregunta.add(new SelectItem(subclasePregunga.getIdSubclasePregunta().toString(), subclasePregunga.getNomSubclasePregunta()));
                }
            } catch (Exception ex) {
            }
        }
    }

    public void registrar() throws Exception {
        try {
            pregunta.getCiudad().setIdCiudad(filtro.getCiudadSel());
            pregunta.getLocalidad().setIdLocalidad(filtro.getLocalidadSel());
            pregunta.getEstrato().setIdEstrato(filtro.getEstratoSel());
            pregunta.getTipoInmueble().setIdTipoInmueble(filtro.getTipoInmuebleSel());
            DaoPregunta daoPregunta = new DaoPregunta();
            daoPregunta.registrar(this.pregunta);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO: ", " El registro se realizo correctamente"));
            this.pregunta = new Pregunta();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public List<Pregunta> getListarPregunta() {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            this.listaPregunta = daoPregunta.verFiltro(filtro.getCiudadSel(), filtro.getLocalidadSel(), filtro.getEstratoSel(), filtro.getTipoInmuebleSel());
            return this.listaPregunta;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public void actualizar() throws Exception {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            daoPregunta.editar(this.pregunta);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO:", " Los datos fueron cambiados exitosamente"));

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }
    
    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public List<Pregunta> getListaPregunta() {
        return listaPregunta;
    }

    public void setListaPregunta(List<Pregunta> listaPregunta) {
        this.listaPregunta = listaPregunta;
    }

    public List<SelectItem> getListaTipoPregunta() {
        try {
            ListaTipoPregunta = new ArrayList();
            InterfaceTipoPregunta daoTipoPregunta = new DaoTipoPregunta();
            tipoPreguntas = daoTipoPregunta.verTodo();
            for (TipoPregunta tipoPregunta1 : tipoPreguntas) {
                tipoPregunta = tipoPregunta1;
                this.ListaTipoPregunta.add(new SelectItem(tipoPregunta.getIdTipoPregunta().toString(), tipoPregunta.getNomTipoPregunta()));
            }
        } catch (Exception ex) {
        }
        return ListaTipoPregunta;
    }

    public void setListaTipoPregunta(List<SelectItem> ListaTipoPregunta) {
        this.ListaTipoPregunta = ListaTipoPregunta;
    }

    public TipoPregunta getTipoPregunta() {
        return tipoPregunta;
    }

    public void setTipoPregunta(TipoPregunta tipoPregunta) {
        this.tipoPregunta = tipoPregunta;
    }

    public List<SelectItem> getListaTipoInmueble() {
        try {
            ListaTipoInmueble = new ArrayList();
            InterfaceTipoInmueble daoTipoInmueble = new DaoTipoInmueble();
            tipoInmuebles = daoTipoInmueble.verTodo();
            for (TipoInmueble tipoInmueble1 : tipoInmuebles) {
                tipoInmueble = tipoInmueble1;
                this.ListaTipoInmueble.add(new SelectItem(tipoInmueble.getIdTipoInmueble().toString(), tipoInmueble.getNomTipoInmueble()));
            }
        } catch (Exception ex) {
        }
        return ListaTipoInmueble;
    }

    public void setListaTipoInmueble(List<SelectItem> ListaTipoInmueble) {
        this.ListaTipoInmueble = ListaTipoInmueble;
    }

    public TipoInmueble getTipoInmueble() {
        return tipoInmueble;
    }

    public void setTipoInmueble(TipoInmueble tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    public List<SelectItem> getListaCiudad() {
        return ListaCiudad;
    }

    public void setListaCiudad(List<SelectItem> ListaCiudad) {
        this.ListaCiudad = ListaCiudad;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public List<SelectItem> getListaPais() {
        try {
            ListaPais = new ArrayList();
            InterfacePais daoPais = new DaoPais();
            paises = daoPais.verTodo();
            for (Pais paise : paises) {
                pais = paise;
                this.ListaPais.add(new SelectItem(pais.getIdPais().toString(), pais.getNomPais()));
            }
        } catch (Exception ex) {
        }
        return ListaPais;
    }

    public void setListaPais(List<SelectItem> ListaPais) {
        this.ListaPais = ListaPais;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public ClasePregunta getClasePregunta() {
        return clasePregunta;
    }

    public void setClasePregunta(ClasePregunta clasePregunta) {
        this.clasePregunta = clasePregunta;
    }

    public List<SelectItem> getListaClasePregunta() {
        try {
            ListaClasePregunta = new ArrayList();
            InterfaceClasePregunta daoClasePregunta = new DaoClasePregunta();
            clasePreguntas = daoClasePregunta.verTodo();
            for (ClasePregunta clasePregunta1 : clasePreguntas) {
                clasePregunta = clasePregunta1;
                this.ListaClasePregunta.add(new SelectItem(clasePregunta.getIdClasePregunta().toString(), clasePregunta.getNomClasePregunta()));
            }
        } catch (Exception ex) {
        }
        return ListaClasePregunta;
    }

    public void setListaClasePregunta(List<SelectItem> ListaClasePregunta) {
        this.ListaClasePregunta = ListaClasePregunta;
    }

    public SubclasePregunga getSubclasePregunga() {
        return subclasePregunga;
    }

    public void setSubclasePregunga(SubclasePregunga subclasePregunga) {
        this.subclasePregunga = subclasePregunga;
    }

    public List<SelectItem> getListaSubClasePregunta() {
        return listaSubClasePregunta;
    }

    public void setListaSubClasePregunta(List<SelectItem> listaSubClasePregunta) {
        this.listaSubClasePregunta = listaSubClasePregunta;
    }

    public List<TipoPregunta> getTipoPreguntas() {
        return tipoPreguntas;
    }

    public void setTipoPreguntas(List<TipoPregunta> tipoPreguntas) {
        this.tipoPreguntas = tipoPreguntas;
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }

    public List<TipoInmueble> getTipoInmuebles() {
        return tipoInmuebles;
    }

    public void setTipoInmuebles(List<TipoInmueble> tipoInmuebles) {
        this.tipoInmuebles = tipoInmuebles;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(List<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public List<ClasePregunta> getClasePreguntas() {
        return clasePreguntas;
    }

    public void setClasePreguntas(List<ClasePregunta> clasePreguntas) {
        this.clasePreguntas = clasePreguntas;
    }

    public List<SubclasePregunga> getSubclasePregungas() {
        return subclasePregungas;
    }

    public void setSubclasePregungas(List<SubclasePregunga> subclasePregungas) {
        this.subclasePregungas = subclasePregungas;
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public List<SelectItem> getListaLocalidad() {
        return ListaLocalidad;
    }

    public void setListaLocalidad(List<SelectItem> ListaLocalidad) {
        this.ListaLocalidad = ListaLocalidad;
    }

    public List<SelectItem> getListaEstrato() {
        return ListaEstrato;
    }

    public void setListaEstrato(List<SelectItem> ListaEstrato) {
        this.ListaEstrato = ListaEstrato;
    }

    public Estrato getEstrato() {
        return estrato;
    }

    public void setEstrato(Estrato estrato) {
        this.estrato = estrato;
    }

    public List<Estrato> getEstratos() {
        return estratos;
    }

    public void setEstratos(List<Estrato> estratos) {
        this.estratos = estratos;
    }

    public MbSfiltroPreguntas getFiltro() {
        return filtro;
    }

    public void setFiltro(MbSfiltroPreguntas filtro) {
        this.filtro = filtro;
    }
}
