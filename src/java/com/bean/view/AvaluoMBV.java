/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import clases.Encriptar;
import clases.EnvioCorreo;
import clases.Pago;
import com.bean.session.MbSlogin;
import com.dao.DaoCiudad;
import com.dao.DaoEstrato;
import com.dao.DaoIntercepto;
import com.dao.DaoLocalidad;
import com.dao.DaoPais;
import com.dao.DaoPregunta;
import com.dao.DaoTipoInmueble;
import com.dao.DaoUsuario;
import com.entidad.Ciudad;
import com.entidad.ClasePregunta;
import com.entidad.Estrato;
import com.entidad.Intercepto;
import com.entidad.Localidad;
import com.entidad.Pais;
import com.entidad.Pregunta;
import com.entidad.SubclasePregunga;
import com.entidad.TipoInmueble;
import com.entidad.TipoPregunta;
import com.entidad.Usuario;
import com.interfaces.InterfaceCiudad;
import com.interfaces.InterfaceEstrato;
import com.interfaces.InterfaceIntercepto;
import com.interfaces.InterfaceLocalidad;
import com.interfaces.InterfacePais;
import com.interfaces.InterfaceTipoInmueble;
import com.interfaces.InterfaceUsuario;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;

/**
 *
 * @author AnchorPriceGroup
 */
@ManagedBean
@SessionScoped
public class AvaluoMBV implements Serializable {

    private Pregunta pregunta;
    private List<Pregunta> listaHabitacione;
    private List<Pregunta> listaBanos;
    private List<Pregunta> listaGaraje;
    private List<Pregunta> listaOtra;
    private List<Pregunta> listaAdicional;
    private List<Pregunta> listaGeografica;
    private List<SelectItem> ListaTipoPregunta;
    private TipoPregunta tipoPregunta;
    private List<TipoPregunta> tipoPreguntas;
    private List<SelectItem> ListaPais;
    private Pais pais;
    private List<Pais> paises;
    private List<SelectItem> ListaTipoInmueble;
    private TipoInmueble tipoInmueble;
    private List<TipoInmueble> tipoInmuebles;
    private List<SelectItem> ListaCiudad;
    private Ciudad ciudad;
    private List<Ciudad> ciudades;
    private ClasePregunta clasePregunta;
    private List<SelectItem> ListaClasePregunta;
    private List<ClasePregunta> clasePreguntas;
    private SubclasePregunga subclasePregunga;
    private List<SelectItem> listaSubClasePregunta;
    private List<SubclasePregunga> subclasePregungas;
    private Integer res;
    private Integer resFinal;
    private List<SelectItem> ListaLocalidad;
    private Localidad localidad;
    private List<Localidad> localidades;
    private List<SelectItem> ListaEstrato;
    private Estrato estrato;
    private List<Estrato> estratos;
    private Intercepto intercepto;
    @ManagedProperty("#{mbSlogin}")
    private MbSlogin mbSlogin;
    private int respuesta;
    private String finalidadAvaluo;
    private Integer metrosCuadrados;
    private Integer valorAvaluo;
    private String direccion;
    private String matricula;
    private String barrio;
    private String nomConjunto;
    private Calendar fecha1 = Calendar.getInstance();
    private int ano1 = 0;
    private int ano = 0;
    private int mes1 = 0;
    private int mes = 0;
    private int dia1 = 0;
    private int dia = 0;
    private int horaA = 0;
    private int hora2 = 0;
    private int minA = 0;
    private int min = 0;
    private String value1;
    private Integer pisoHabitacion;
    private String piso1;
    private Integer paredHabitacion;
    private String pared1;
    private Integer pisoBano;
    private String piso2;
    private Integer paredBano;
    private String pared2;
    private Integer tipoInstalacion;
    private String tipoIns;
    private Integer tipoApartamento;
    private String tipoApto;
    private boolean jacuzzi;
    private boolean sauna;
    private boolean turco;
    private boolean cubierto;
    private boolean masGarajes;
    private boolean citofono;
    private boolean gimnasio;
    private boolean shut;
    private boolean zonasVerdes;
    private boolean canchas;
    private boolean oficinaNegocio;
    private boolean terrazaComunal;
    private boolean circuitoTv;
    private boolean salaInternet;
    private boolean vigilancia;
    private boolean canchaSquash;
    private boolean cuatoEscoltas;
    private boolean jardinInfantil;
    private boolean piscina;
    private boolean vistaPanoramica;
    private boolean canchaTenis;
    private boolean conjuntoCerrado;
    private boolean jaulaGolf;
    private boolean plantaElectrica;
    private boolean SalonJuego;
    private boolean residencial;
    private boolean comercial;
    private boolean campestre;
    private boolean industrial;
    private boolean principal;
    private boolean parques;
    private Integer areaGaraje;
    private Integer numBloque;
    private Integer numVentanas;
    private Integer numApto;
    private String nombreArchivo;
    private Pago pago;
    private Usuario usuario;
    private int contador;

    public AvaluoMBV() throws Exception {
        ListaTipoPregunta = new ArrayList<SelectItem>();
        ListaCiudad = new ArrayList<SelectItem>();
        ListaPais = new ArrayList<SelectItem>();
        ListaClasePregunta = new ArrayList<SelectItem>();
        listaSubClasePregunta = new ArrayList<SelectItem>();
        ListaLocalidad = new ArrayList<SelectItem>();
        ListaEstrato = new ArrayList<SelectItem>();
        ListaTipoInmueble = new ArrayList<SelectItem>();
        this.tipoInmueble = new TipoInmueble();
        this.tipoPregunta = new TipoPregunta();
        this.pago = new Pago();
        this.ciudad = new Ciudad();
        this.pais = new Pais();
        this.clasePregunta = new ClasePregunta();
        this.subclasePregunga = new SubclasePregunga();
        this.localidad = new Localidad();
        this.estrato = new Estrato();
        this.tipoInmueble = new TipoInmueble();
        this.pregunta = new Pregunta(ciudad, clasePregunta, estrato, localidad, subclasePregunga, tipoInmueble, tipoPregunta);
        ano1 = fecha1.get(Calendar.YEAR);
        mes1 = fecha1.get(Calendar.MONTH);
        dia1 = fecha1.get(Calendar.DAY_OF_MONTH);
        contador = 0;
    }

    public void cambiarPreguntaLocalidad() {
        if (pregunta.getLocalidad().getIdLocalidad() == null && pregunta.getLocalidad().getIdLocalidad().equals("0")) {
            ListaEstrato = new ArrayList();
        } else {
            ListaEstrato = new ArrayList();
            try {
                InterfaceEstrato daoEstrato = new DaoEstrato();
                estratos = daoEstrato.verPorPais(pregunta.getLocalidad().getIdLocalidad());
                for (Estrato estrato1 : estratos) {
                    estrato = estrato1;
                    this.ListaEstrato.add(new SelectItem(estrato.getIdEstrato().toString(), estrato.getNomEstrato(), estrato.getVariable().toString()));
                }
            } catch (Exception ex) {
            }
        }
    }

    public void cambiarPreguntaCiudad() {
        if (pregunta.getCiudad().getIdCiudad() == null && pregunta.getCiudad().getIdCiudad().equals("0")) {
            ListaLocalidad = new ArrayList();
        } else {
            ListaLocalidad = new ArrayList();
            try {
                InterfaceLocalidad daoLocalidad = new DaoLocalidad();
                localidades = daoLocalidad.verPorCiudad(pregunta.getCiudad().getIdCiudad());
                for (Localidad localidade : localidades) {
                    localidad = localidade;
                    this.ListaLocalidad.add(new SelectItem(localidad.getIdLocalidad().toString(), localidad.getNomLocalidad()));
                }
            } catch (Exception ex) {
            }
        }
    }

    public void cambiarPreguntaPais() {
        if (pais.getIdPais() == null && pais.getIdPais().equals("0")) {
            ListaCiudad = new ArrayList();
        } else {
            ListaCiudad = new ArrayList();
            try {
                InterfaceCiudad daoCiudad = new DaoCiudad();
                ciudades = daoCiudad.verPorPais(pais.getIdPais());
                for (Ciudad ciudade : ciudades) {
                    ciudad = ciudade;
                    this.ListaCiudad.add(new SelectItem(ciudad.getIdCiudad().toString(), ciudad.getNomCiudad()));
                }
            } catch (Exception ex) {
            }
        }
    }

    public String agendarCita() {
        String hora = "";
        if (value1.equals("uno")) {
            dia1 = dia1 + 1;
            mes1 = mes1 + 1;
            hora = "8:00 am - 11:00am";
        }
        if (value1.equals("dos")) {
            dia1 = dia1 + 1;
            mes1 = mes1 + 1;
            hora = "2:00 pm - 5:00pm";
        }
        if (value1.equals("tres")) {
            dia1 = dia1 + 2;
            mes1 = mes1 + 1;
            hora = "8:00 am - 11:00am";
        }
        try {
            InterfaceUsuario daoUsuario = new DaoUsuario();
            Usuario usuario = daoUsuario.verPorEmail(this.mbSlogin.getEmail());
            Properties props = new Properties();
            props.put("mail.smtp.host", "66.7.219.16");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "certificado@avaluofacil.com");
            props.setProperty("mail.smtp.auth", "true");
            Session session = Session.getDefaultInstance(props, null);
            BodyPart texto = new MimeBodyPart();
            texto.setText("El Señor(a)" + usuario.getNombre() + "\r\n\r\n\r\nAvaluofacil.com tiene el pacer de confimarle el agendamiento de su cita para el " + dia1 + "/" + mes1 + "/" + ano1 + " en el horario de " + hora + " en la dirección" + direccion + ". la misma sera efectiva una vez confirmado el pago a través de la plataforma segura PayU" + "\r\n\r\n\r\nLe recomendamos tener disponible el original del certificado de tradición y libertad del inmueble." + "\r\n\r\n\r\natt:\r\n\r\n" + "Equipo innovador de Avaluofacil.com");
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("certificado@avaluofacil.com"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(usuario.getEmail()));
            message.addRecipient(
                    Message.RecipientType.BCC,
                    new InternetAddress("pipe0789@gmail.com"));
            message.setSubject("Agendamiento cita avaluofacil.com");
            message.setContent(multiParte);

            Transport t = session.getTransport("smtp");
            t.connect("certificado@avaluofacil.com", "Le0nar201");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO: ", " Le hemos enviado un correo con el agendamiento de la cita"));
            if (usuario.getTipoPersona().getId() == 5) {
                enviarAvaluo();
                return "/index";
            } else {
                pago.apiKey = "39mdmeb39f93mpltk1plgcv9ff";
                pago.merchantId = "519120";
                pago.valorAvaluo = valorAvaluo;
                pago.currency = "COP";
                pago.buyerMailer = mbSlogin.getEmail();
                pago.referenceCode = nombreArchivo;
                pago.firmaCadena = pago.apiKey + "~" + pago.merchantId + "~" + pago.referenceCode + "~" + pago.valorAvaluo + "~" + pago.currency;
                pago.firmaCreada = Encriptar.MD5(pago.firmaCadena);
                return "/avaluo/confirmarPago";
            }
        } catch (DocumentException e) {
            System.out.println(e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + e.getMessage()));
        } catch (IOException ex) {
            Logger.getLogger(AvaluoMBV.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        } catch (Exception ex) {
            Logger.getLogger(AvaluoMBV.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String calcularPrecio() {
        InterfaceEstrato daoEstrato = new DaoEstrato();
        Estrato estratoPrecio = daoEstrato.verPorEstrato(pregunta.getEstrato().getIdEstrato());
        valorAvaluo = calcularRespuesta(finalidadAvaluo, estratoPrecio.getNomEstrato(), metrosCuadrados);
        return "/avaluo/formulario";
    }

    public String enviarAvaluo() throws Exception {
        InterfaceUsuario daoUsuario = new DaoUsuario();
        Usuario usuario = daoUsuario.verPorEmail(this.mbSlogin.getEmail());
        EnvioCorreo.correoElectronico(mbSlogin.getEmail(), usuario.getNombre(), nombreArchivo);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "CORRECTO: ", " Su avalúo fue enviado al correo  " + usuario.getEmail()));
        mbSlogin.cerrarSesion();
        return "/index";
    }

    public String redirecionGratis() {
        return "/especial/avaluofacil";
    }

    public String redirecionPago() throws Exception {
        contador = 1;
        return "/avaluo/avaluofacil";
    }

    public String cerrarSession() throws Exception {
        mbSlogin.cerrarSesion();
        return "/index";
    }

    public String realizarAvaluo() throws Exception {
        switch (pisoHabitacion) {
            case 1:
                piso1 = "Madera";
                break;
            case 2:
                piso1 = "Mármol";
                break;
            case 3:
                piso1 = "Obra Gris";
                break;
            case 4:
                piso1 = "Alfombra";
                break;
            case 5:
                piso1 = "Baldosa";
                break;
            case 6:
                piso1 = "Cerámica";
                break;
            case 7:
                piso1 = "Porcelanato";
                break;
            case 8:
                piso1 = "Laminado";
                break;
        }
        switch (pisoBano) {
            case 1:
                piso2 = "Mármol";
                break;
            case 2:
                piso2 = "Baldosa";
                break;
            case 3:
                piso2 = "Cerámica";
                break;
            case 4:
                piso2 = "Porcelanato";
                break;
            case 5:
                piso2 = "Laminado";
                break;
        }
        switch (paredHabitacion) {
            case 1:
                pared1 = "Pintadas";
                break;
            case 2:
                pared1 = "Estucadas";
                break;
            case 3:
                pared1 = "Divisiones Dry Wall";
                break;
        }
        switch (paredBano) {
            case 1:
                pared2 = "Pintadas";
                break;
            case 2:
                pared2 = "Estucadas";
                break;
            case 3:
                pared2 = "Baldosa";
                break;
        }
        switch (tipoInstalacion) {
            case 1:
                tipoIns = "Natural";
                break;
            case 2:
                tipoIns = "Propano";
                break;
            case 3:
                tipoIns = "Niguna";
                break;
        }
        switch (tipoApartamento) {
            case 1:
                tipoApto = "Dúplex";
                break;
            case 2:
                tipoApto = "Penthouse";
                break;
            case 3:
                tipoApto = "Loft";
                break;
            case 4:
                tipoApto = "Estándar";
                break;
        }
        try {
            InterfaceIntercepto daoIntercepto = new DaoIntercepto();
            InterfacePais daoPais = new DaoPais();
            InterfaceCiudad daoCiudad = new DaoCiudad();
            InterfaceLocalidad daoLocalidad = new DaoLocalidad();
            InterfaceEstrato daoEstrato = new DaoEstrato();
            InterfaceTipoInmueble daoTipoInmueble = new DaoTipoInmueble();
            InterfaceUsuario daoUsuario = new DaoUsuario();
            Usuario usuario = daoUsuario.verPorEmail(this.mbSlogin.getEmail());
            pais = daoPais.verPorPais(pais.getIdPais());
            ciudad = daoCiudad.verPorCiudad(pregunta.getCiudad().getIdCiudad());
            localidad = daoLocalidad.verPorLocalidad(pregunta.getLocalidad().getIdLocalidad());
            estrato = daoEstrato.verPorEstrato(pregunta.getEstrato().getIdEstrato());
            tipoInmueble = daoTipoInmueble.verPorTipoInmueble(pregunta.getTipoInmueble().getIdTipoInmueble());
            intercepto = daoIntercepto.verPorIntercepto(pais.getIdPais(), ciudad.getIdCiudad(), localidad.getIdLocalidad(), estrato.getIdEstrato(), tipoInmueble.getIdTipoInmueble());
            resFinal = intercepto.getConstante() + estrato.getVariable();
            Calendar fecha = Calendar.getInstance();
            ano = fecha.get(Calendar.YEAR);
            mes = fecha.get(Calendar.MONTH);
            dia = fecha.get(Calendar.DAY_OF_MONTH);
            hora2 = fecha.get(Calendar.HOUR);
            min = fecha.get(Calendar.MINUTE);
            Document doc = new Document(PageSize.LETTER);
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String carpetaAvaluo = (String) servletContext.getRealPath("/Avaluos");
            String imagenes = (String) servletContext.getRealPath("/resources/imagenes");
            nombreArchivo = usuario.getNumeroDocumento().toString() + dia + (mes + 1) + ano + hora2 + min;
            FileOutputStream archivo = new FileOutputStream(carpetaAvaluo + "/" + nombreArchivo + ".pdf");
            PdfWriter archivoWriter = PdfWriter.getInstance(doc, archivo);
            Font font1 = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
            Font font2 = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
            Font font3 = new Font(Font.FontFamily.HELVETICA, 11, Font.NORMAL);
            Paragraph linia1 = new Paragraph();
            Paragraph linia2 = new Paragraph();
            Paragraph linia3 = new Paragraph();
            Paragraph linia4 = new Paragraph();
            Paragraph caracInmuebleId1 = new Paragraph();
            Paragraph caracInmuebleId2 = new Paragraph();
            Paragraph caracInmuebleId3 = new Paragraph();
            Paragraph caracInmuebleId4 = new Paragraph();
            Paragraph caracAdicionales = new Paragraph();
            Paragraph caracGeograficas = new Paragraph();
            Image logo = Image.getInstance(imagenes + "/" + "logo.png");
            Image rna = Image.getInstance(imagenes + "/" + "rna.png");
            doc.open();
            doc.add(logo);
            doc.add(new Paragraph("INFORMACION BASICA", font1));
            Chunk nom = new Chunk("Nombre: ", font2);
            Chunk nom2 = new Chunk(usuario.getNombre(), font3);
            Chunk num = new Chunk("         Numero de cedula: ", font2);
            Chunk num2 = new Chunk(usuario.getNumeroDocumento().toString(), font3);
            Chunk fec = new Chunk("         fecha del avaluo: ", font2);
            Chunk fec2 = new Chunk(dia + "/" + (mes + 1) + "/" + ano + " ", font3);
            Chunk numa = new Chunk("         Numero de Avaluo: ", font2);
            Chunk numa2 = new Chunk(usuario.getNumeroDocumento().toString() + dia + (mes + 1) + ano + hora2 + min, font3);
            Chunk pa = new Chunk("Pais: ", font2);
            Chunk pa2 = new Chunk(pais.getNomPais() + " ", font3);
            Chunk ciu = new Chunk("               Ciudad: ", font2);
            Chunk ciu2 = new Chunk(ciudad.getNomCiudad() + " ", font3);
            Chunk loc = new Chunk("               Localidad: ", font2);
            Chunk loc2 = new Chunk(localidad.getNomLocalidad() + " ", font3);
            Chunk est = new Chunk("               Estrato: ", font2);
            Chunk est2 = new Chunk(estrato.getNomEstrato() + " ", font3);
            Chunk sec = new Chunk("Sector: ", font2);
            Chunk sec2 = new Chunk("Urbano", font3);
            Chunk met = new Chunk("          Metodología: ", font2);
            Chunk met2 = new Chunk("Comparación de  Mercado", font3);
            Chunk es = new Chunk("           Estado Avalúo: ", font2);
            Chunk es2 = new Chunk("Aprobado", font3);
            Chunk dir = new Chunk("Dirección: ", font2);
            Chunk dir2 = new Chunk(direccion, font3);
            Chunk nbar = new Chunk("           Nombre común del barrio: ", font2);
            Chunk nbar2 = new Chunk(barrio, font3);
            Chunk nomconj = new Chunk("           Nombre del conjunto: ", font2);
            Chunk nomconj2 = new Chunk(nomConjunto, font3);
            Chunk matri = new Chunk("           Número de matricula inmobiliaria: ", font2);
            Chunk matri2 = new Chunk(matricula, font3);
            linia1.add(nom);
            linia1.add(nom2);
            linia1.add(num);
            linia1.add(num2);
            linia1.add(fec);
            linia1.add(fec2);
            linia1.add(numa);
            linia1.add(numa2);
            doc.add(linia1);
            linia2.add(pa);
            linia2.add(pa2);
            linia2.add(ciu);
            linia2.add(ciu2);
            linia2.add(loc);
            linia2.add(loc2);
            linia2.add(est);
            linia2.add(est2);
            doc.add(linia2);
            linia3.add(sec);
            linia3.add(sec2);
            linia3.add(met);
            linia3.add(met2);
            linia3.add(es);
            linia3.add(es2);
            doc.add(linia3);
            linia4.add(dir);
            linia4.add(dir2);
            linia4.add(nbar);
            linia4.add(nbar2);
            linia4.add(nomconj);
            linia4.add(nomconj2);
            linia4.add(matri);
            linia4.add(matri2);
            doc.add(linia4);
            Iterator<Pregunta> itHabitacione = listaHabitacione.listIterator();
            Iterator<Pregunta> itBano = listaBanos.listIterator();
            Iterator<Pregunta> itGaraje = listaGaraje.listIterator();
            Iterator<Pregunta> itOtra = listaOtra.listIterator();
            Iterator<Pregunta> itAdicional = listaAdicional.listIterator();
            Iterator<Pregunta> itGeografica = listaGeografica.listIterator();
            Chunk pisoh = new Chunk("El piso es en: ", font2);
            Chunk pisoh1 = new Chunk(piso1 + "      ", font3);
            caracInmuebleId1.add(pisoh);
            caracInmuebleId1.add(pisoh1);
            Chunk paredh = new Chunk("La pared es en: ", font2);
            Chunk paredh1 = new Chunk(pared1 + "      ", font3);
            caracInmuebleId1.add(paredh);
            caracInmuebleId1.add(paredh1);
            Chunk pisob = new Chunk("El piso es en: ", font2);
            Chunk pisob1 = new Chunk(piso2 + "      ", font3);
            caracInmuebleId2.add(pisob);
            caracInmuebleId2.add(pisob1);
            Chunk paredb = new Chunk("La pared es en: ", font2);
            Chunk paredb1 = new Chunk(pared2 + "      ", font3);
            caracInmuebleId2.add(paredb);
            caracInmuebleId2.add(paredb1);

            Chunk jac = new Chunk("Jacuzzi: ", font2);
            caracInmuebleId2.add(jac);
            if (jacuzzi == true) {
                Chunk jac1 = new Chunk("Si" + "      ", font3);
                caracInmuebleId2.add(jac1);
            } else {
                Chunk jac1 = new Chunk("No" + "      ", font3);
                caracInmuebleId2.add(jac1);
            }

            Chunk sau = new Chunk("Sauna: ", font2);
            caracInmuebleId2.add(sau);
            if (sauna == true) {
                Chunk sau1 = new Chunk("Si" + "      ", font3);
                caracInmuebleId2.add(sau1);
            } else {
                Chunk sau1 = new Chunk("No" + "      ", font3);
                caracInmuebleId2.add(sau1);
            }

            Chunk tur = new Chunk("Turco: ", font2);
            caracInmuebleId2.add(tur);
            if (turco == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracInmuebleId2.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracInmuebleId2.add(tur1);
            }

            Chunk cub = new Chunk("Cubierto(s): ", font2);
            caracInmuebleId3.add(cub);
            if (cubierto == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracInmuebleId3.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracInmuebleId3.add(tur1);
            }

            Chunk masGa = new Chunk("Si tiene dos o más garajes, ¿Son seguidos?: ", font2);
            caracInmuebleId3.add(masGa);
            if (masGarajes == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracInmuebleId3.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracInmuebleId3.add(tur1);
            }
            Chunk aGar = new Chunk("Área garaje: ", font2);
            Chunk aGar1 = new Chunk(areaGaraje + "      ", font3);
            caracInmuebleId3.add(aGar);
            caracInmuebleId3.add(aGar1);

            Chunk nblo = new Chunk("Número de bloque: ", font2);
            Chunk nblo1 = new Chunk(numBloque + "      ", font3);
            caracInmuebleId4.add(nblo);
            caracInmuebleId4.add(nblo1);

            Chunk napto = new Chunk("Número Apartamento: ", font2);
            Chunk napto1 = new Chunk(numApto + "      ", font3);
            caracInmuebleId4.add(napto);
            caracInmuebleId4.add(napto1);

            Chunk nvent = new Chunk("Número de Ventanas: ", font2);
            Chunk nvet1 = new Chunk(numVentanas + "      ", font3);
            caracInmuebleId4.add(nvent);
            caracInmuebleId4.add(nvet1);

            Chunk tipoins = new Chunk("Tipo de intalacion de gas: ", font2);
            Chunk tipoins1 = new Chunk(tipoIns + "      ", font3);
            caracInmuebleId4.add(tipoins);
            caracInmuebleId4.add(tipoins1);
            Chunk tipoInm = new Chunk("Tipo de apartamento: ", font2);
            Chunk tipoInm1 = new Chunk(tipoApto + "      ", font3);
            caracInmuebleId4.add(tipoInm);
            caracInmuebleId4.add(tipoInm1);

            Chunk cito = new Chunk("Citófono: ", font2);
            caracAdicionales.add(cito);
            if (citofono == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk gim = new Chunk("Gimnasio: ", font2);
            caracAdicionales.add(gim);
            if (gimnasio == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk shut1 = new Chunk("Shut de Basuras: ", font2);
            caracAdicionales.add(shut1);
            if (shut == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk zvr = new Chunk("Zonas Verdes: ", font2);
            caracAdicionales.add(zvr);
            if (zonasVerdes == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk can = new Chunk("Canchas múltiples: ", font2);
            caracAdicionales.add(can);
            if (canchas == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk ofi = new Chunk("Oficina de Negocios: ", font2);
            caracAdicionales.add(ofi);
            if (oficinaNegocio == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk terr = new Chunk("Terraza Comunal: ", font2);
            caracAdicionales.add(terr);
            if (terrazaComunal == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk cir = new Chunk("Circuito de TV: ", font2);
            caracAdicionales.add(cir);
            if (circuitoTv == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk sal = new Chunk("Sala de internet: ", font2);
            caracAdicionales.add(sal);
            if (salaInternet == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk vig = new Chunk("Vigilancia 24 horas: ", font2);
            caracAdicionales.add(cito);
            if (vigilancia == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk cans = new Chunk("Cancha de Squash: ", font2);
            caracAdicionales.add(cans);
            if (canchaSquash == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk cuar = new Chunk("Cuarto de escoltas: ", font2);
            caracAdicionales.add(cuar);
            if (cuatoEscoltas == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk jinf = new Chunk("Jardín Infantil: ", font2);
            caracAdicionales.add(jinf);
            if (jardinInfantil == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk pisc = new Chunk("Piscina: ", font2);
            caracAdicionales.add(pisc);
            if (piscina == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk vis = new Chunk("Vista Panorámica: ", font2);
            caracAdicionales.add(vis);
            if (vistaPanoramica == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk conj = new Chunk("Conjunto Cerrado: ", font2);
            caracAdicionales.add(conj);
            if (conjuntoCerrado == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk jgolf = new Chunk("Jaula de Golf: ", font2);
            caracAdicionales.add(jgolf);
            if (jaulaGolf == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk pele = new Chunk("Planta Eléctrica: ", font2);
            caracAdicionales.add(pele);
            if (plantaElectrica == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk salj = new Chunk("Salon de Juegos: ", font2);
            caracAdicionales.add(salj);
            if (SalonJuego == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracAdicionales.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracAdicionales.add(tur1);
            }

            Chunk resi = new Chunk("Está ubicado en zona rescidencial: ", font2);
            caracGeograficas.add(resi);
            if (residencial == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracGeograficas.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracGeograficas.add(tur1);
            }

            Chunk comer = new Chunk("Está ubicado en zona comercial: ", font2);
            caracGeograficas.add(comer);
            if (comercial == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracGeograficas.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracGeograficas.add(tur1);
            }

            Chunk cam = new Chunk("Está ubicado en zona campestre: ", font2);
            caracGeograficas.add(cam);
            if (campestre == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracGeograficas.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracGeograficas.add(tur1);
            }

            Chunk ind = new Chunk("Está ubicado en zona industrial: ", font2);
            caracGeograficas.add(ind);
            if (industrial == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracGeograficas.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracGeograficas.add(tur1);
            }

            Chunk prin = new Chunk("Está ubicado sobre vía principal: ", font2);
            caracGeograficas.add(prin);
            if (principal == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracGeograficas.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracGeograficas.add(tur1);
            }

            Chunk parq = new Chunk("Está ubicado al lado de un parque: ", font2);
            caracGeograficas.add(parq);
            if (parques == true) {
                Chunk tur1 = new Chunk("Si" + "      ", font3);
                caracGeograficas.add(tur1);
            } else {
                Chunk tur1 = new Chunk("No" + "      ", font3);
                caracGeograficas.add(tur1);
            }

            while (itHabitacione.hasNext()) {
                pregunta = itHabitacione.next();
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 1) {
                    if (pregunta.getRespuesta() == 0) {
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(" n/a" + "      ", font3);
                        caracInmuebleId1.add(con);
                        caracInmuebleId1.add(con1);
                    } else {
                        res = pregunta.getRespuesta() * pregunta.getVariable();
                        resFinal = resFinal + res;
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(pregunta.getRespuesta() + "      ", font3);
                        caracInmuebleId1.add(con);
                        caracInmuebleId1.add(con1);
                    }
                }
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 2) {
                    if (pregunta.isRespuestab() == true) {
                        resFinal = resFinal + pregunta.getVariable();
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" Si" + "      ", font3);
                        caracInmuebleId1.add(bool);
                        caracInmuebleId1.add(bool1);
                    } else {
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" No" + "      ", font3);
                        caracInmuebleId1.add(bool);
                        caracInmuebleId1.add(bool1);
                    }
                }
            }
            while (itBano.hasNext()) {
                pregunta = itBano.next();
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 1) {
                    if (pregunta.getRespuesta() == 0) {
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(" n/a" + "      ", font3);
                        caracInmuebleId2.add(con);
                        caracInmuebleId2.add(con1);
                    } else {
                        res = pregunta.getRespuesta() * pregunta.getVariable();
                        resFinal = resFinal + res;
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(pregunta.getRespuesta() + "      ", font3);
                        caracInmuebleId2.add(con);
                        caracInmuebleId2.add(con1);
                    }
                }
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 2) {
                    if (pregunta.isRespuestab() == true) {
                        resFinal = resFinal + pregunta.getVariable();
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" Si" + "      ", font3);
                        caracInmuebleId2.add(bool);
                        caracInmuebleId2.add(bool1);
                    } else {
                        Chunk bool = new Chunk(pregunta.getPregunta(), font2);
                        Chunk bool1 = new Chunk(" No" + "      ", font3);
                        caracInmuebleId2.add(bool);
                        caracInmuebleId2.add(bool1);
                    }
                }
            }
            while (itGaraje.hasNext()) {
                pregunta = itGaraje.next();
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 1) {
                    if (pregunta.getRespuesta() == 0) {
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(" n/a" + "      ", font3);
                        caracInmuebleId3.add(con);
                        caracInmuebleId3.add(con1);
                    } else {
                        res = pregunta.getRespuesta() * pregunta.getVariable();
                        resFinal = resFinal + res;
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(pregunta.getRespuesta() + "      ", font3);
                        caracInmuebleId3.add(con);
                        caracInmuebleId3.add(con1);
                    }
                }
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 2) {
                    if (pregunta.isRespuestab() == true) {
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" Si" + "      ", font3);
                        caracInmuebleId3.add(bool);
                        caracInmuebleId3.add(bool1);
                    } else {
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" No" + "      ", font3);
                        caracInmuebleId3.add(bool);
                        caracInmuebleId3.add(bool1);
                    }
                }
            }
            while (itOtra.hasNext()) {
                pregunta = itOtra.next();
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 1) {
                    if (pregunta.getRespuesta() == 0) {
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(" n/a" + "      ", font3);
                        caracInmuebleId4.add(con);
                        caracInmuebleId4.add(con1);
                    } else {
                        res = pregunta.getRespuesta() * pregunta.getVariable();
                        resFinal = resFinal + res;
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(pregunta.getRespuesta() + "      ", font3);
                        caracInmuebleId4.add(con);
                        caracInmuebleId4.add(con1);
                    }
                }
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 2) {
                    if (pregunta.isRespuestab() == true) {
                        resFinal = resFinal + pregunta.getVariable();
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" Si" + "      ", font3);
                        caracInmuebleId4.add(bool);
                        caracInmuebleId4.add(bool1);
                    } else {
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" No" + "      ", font3);
                        caracInmuebleId4.add(bool);
                        caracInmuebleId4.add(bool1);
                    }
                }
            }
            while (itAdicional.hasNext()) {
                pregunta = itAdicional.next();
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 1) {
                    if (pregunta.getRespuesta() == 0) {
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(" n/a" + "      ", font3);
                        caracAdicionales.add(con);
                        caracAdicionales.add(con1);
                    } else {
                        res = pregunta.getRespuesta() * pregunta.getVariable();
                        resFinal = resFinal + res;
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(pregunta.getRespuesta() + "      ", font3);
                        caracAdicionales.add(con);
                        caracAdicionales.add(con1);
                    }
                }
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 2) {
                    if (pregunta.isRespuestab() == true) {
                        resFinal = resFinal + pregunta.getVariable();
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" Si" + "      ", font3);
                        caracAdicionales.add(bool);
                        caracAdicionales.add(bool1);
                    } else {
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" No" + "      ", font3);
                        caracAdicionales.add(bool);
                        caracAdicionales.add(bool1);
                    }
                }
            }
            while (itGeografica.hasNext()) {
                pregunta = itGeografica.next();
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 1) {
                    if (pregunta.getRespuesta() == 0) {
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(" n/a" + "      ", font3);
                        caracGeograficas.add(con);
                        caracGeograficas.add(con1);
                    } else {
                        res = pregunta.getRespuesta() * pregunta.getVariable();
                        resFinal = resFinal + res;
                        Chunk con = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk con1 = new Chunk(pregunta.getRespuesta() + "      ", font3);
                        caracGeograficas.add(con);
                        caracGeograficas.add(con1);
                    }
                }
                if (pregunta.getTipoPregunta().getIdTipoPregunta() == 2) {
                    if (pregunta.isRespuestab() == true) {
                        resFinal = resFinal + pregunta.getVariable();
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" Si" + "      ", font3);
                        caracGeograficas.add(bool);
                        caracGeograficas.add(bool1);
                    } else {
                        Chunk bool = new Chunk(pregunta.getPregunta() + " ", font2);
                        Chunk bool1 = new Chunk(" No" + "      ", font3);
                        caracGeograficas.add(bool);
                        caracGeograficas.add(bool1);
                    }
                }
            }
            doc.add(new Paragraph("CARACTERISTICAS DEL INMUEBLE", font1));
            doc.add(new Paragraph("HABITACIONES", font2));
            doc.add(caracInmuebleId1);
            doc.add(new Paragraph("BAÑOS", font2));
            doc.add(caracInmuebleId2);
            doc.add(new Paragraph("GARAJES", font2));
            doc.add(caracInmuebleId3);
            doc.add(new Paragraph("OTRAS", font2));
            doc.add(caracInmuebleId4);
            doc.add(new Paragraph("CARACTERISTICAS ADICIONALES", font1));
            doc.add(caracAdicionales);
            doc.add(new Paragraph("CARACTERISTICAS GEOGRAFICAS", font1));
            doc.add(caracGeograficas);
            String avaluoFinal = integerFormat(resFinal);
            String avaluoRespuesta = "el valor de tu avaluo es : " + avaluoFinal;
            doc.add(new Paragraph(avaluoRespuesta));
            rna.setAbsolutePosition(440, 670);
            doc.add(rna);
            Chunk footer = new Chunk("Avalúo Fácil garantiza que el presente avalúo, realizado bajo la metodología de comparacion de mercados, cumple la norma de tener un coeficiente de asimetría inferior a 7.5%");
            doc.add(footer);
            doc.close();
            horaA = hora2;
            minA = min;
            if (finalidadAvaluo.equals("2")) {
                return "/avaluo/agendar";
            }
            if (usuario.getTipoPersona().getId() == 5 || usuario.getTipoPersona().getId() == 6 && contador == 0) {
                enviarAvaluo();
                return "/index";
            } else {
                pago.apiKey = "39mdmeb39f93mpltk1plgcv9ff";
                pago.merchantId = "519120";
                pago.valorAvaluo = valorAvaluo;
                pago.currency = "COP";
                pago.buyerMailer = mbSlogin.getEmail();
                pago.referenceCode = nombreArchivo;
                pago.firmaCadena = pago.apiKey + "~" + pago.merchantId + "~" + pago.referenceCode + "~" + pago.valorAvaluo + "~" + pago.currency;
                pago.firmaCreada = Encriptar.MD5(pago.firmaCadena);
                return "/avaluo/confirmarPago";
            }
        } catch (DocumentException e) {
            System.out.println(e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + e.getMessage()));
        } catch (IOException ex) {
            Logger.getLogger(AvaluoMBV.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
        return "";
    }

    private static String integerFormat(float a) {
        DecimalFormat df = new DecimalFormat("$#,###.###");
        String s = df.format(a);
        return s;
    }

    public int calcularRespuesta(String finalidad, String estratoLista, int metrosCuadrados) {
        if (finalidad.equals("1")) {
            if (estratoLista.equals("1")) {
                respuesta = 35000 + (metrosCuadrados * 180);
            }
            if (estratoLista.equals("2")) {
                respuesta = 35000 + (metrosCuadrados * 200);
            }
            if (estratoLista.equals("3")) {
                respuesta = 35000 + (metrosCuadrados * 220);
            }
            if (estratoLista.equals("4")) {
                respuesta = 35000 + (metrosCuadrados * 450);
            }
            if (estratoLista.equals("5")) {
                respuesta = 35000 + (metrosCuadrados * 600);
            }
            if (estratoLista.equals("6")) {
                respuesta = 35000 + (metrosCuadrados * 700);
            }
        }
        if (finalidad.equals("2")) {
            if (estratoLista.equals("1")) {
                respuesta = 70000 + (metrosCuadrados * 180);
            }
            if (estratoLista.equals("2")) {
                respuesta = 75000 + (metrosCuadrados * 200);
            }
            if (estratoLista.equals("3")) {
                respuesta = 90000 + (metrosCuadrados * 220);
            }
            if (estratoLista.equals("4")) {
                respuesta = 110000 + (metrosCuadrados * 450);
            }
            if (estratoLista.equals("5")) {
                respuesta = 130000 + (metrosCuadrados * 600);
            }
            if (estratoLista.equals("6")) {
                respuesta = 160000 + (metrosCuadrados * 1400);
            }
        }
        return respuesta;
    }

    public List<Pregunta> getListarHabitacion() {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            this.listaHabitacione = daoPregunta.verPorHabitacione(pregunta.getCiudad().getIdCiudad(), pregunta.getLocalidad().getIdLocalidad(), pregunta.getEstrato().getIdEstrato(), pregunta.getTipoInmueble().getIdTipoInmueble());
            return listaHabitacione;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public List<Pregunta> getListarBano() {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            this.listaBanos = daoPregunta.verPorBano(pregunta.getCiudad().getIdCiudad(), pregunta.getLocalidad().getIdLocalidad(), pregunta.getEstrato().getIdEstrato(), pregunta.getTipoInmueble().getIdTipoInmueble());
            return listaBanos;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public List<Pregunta> getListarGaraje() {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            this.listaGaraje = daoPregunta.verPorGaraje(pregunta.getCiudad().getIdCiudad(), pregunta.getLocalidad().getIdLocalidad(), pregunta.getEstrato().getIdEstrato(), pregunta.getTipoInmueble().getIdTipoInmueble());
            return listaGaraje;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public List<Pregunta> getListarOtra() {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            this.listaOtra = daoPregunta.verPorOtra(pregunta.getCiudad().getIdCiudad(), pregunta.getLocalidad().getIdLocalidad(), pregunta.getEstrato().getIdEstrato(), pregunta.getTipoInmueble().getIdTipoInmueble());
            return listaOtra;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public List<Pregunta> getListarAdicional() {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            this.listaAdicional = daoPregunta.verPorAdicional(pregunta.getCiudad().getIdCiudad(), pregunta.getLocalidad().getIdLocalidad(), pregunta.getEstrato().getIdEstrato(), pregunta.getTipoInmueble().getIdTipoInmueble());
            return listaAdicional;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public List<Pregunta> getListarGeografica() {
        try {
            DaoPregunta daoPregunta = new DaoPregunta();
            this.listaGeografica = daoPregunta.verPorGeografica(pregunta.getCiudad().getIdCiudad(), pregunta.getLocalidad().getIdLocalidad(), pregunta.getEstrato().getIdEstrato(), pregunta.getTipoInmueble().getIdTipoInmueble());
            return listaGeografica;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public List<SelectItem> getListaTipoPregunta() {
        return ListaTipoPregunta;
    }

    public void setListaTipoPregunta(List<SelectItem> ListaTipoPregunta) {
        this.ListaTipoPregunta = ListaTipoPregunta;
    }

    public TipoPregunta getTipoPregunta() {
        return tipoPregunta;
    }

    public void setTipoPregunta(TipoPregunta tipoPregunta) {
        this.tipoPregunta = tipoPregunta;
    }

    public List<SelectItem> getListaPais() {

        try {
            ListaPais = new ArrayList();
            InterfacePais daoPais = new DaoPais();
            paises = daoPais.verTodo();
            for (Pais paise : paises) {
                pais = paise;
                this.ListaPais.add(new SelectItem(pais.getIdPais().toString(), pais.getNomPais()));
            }
            pais = new Pais();
        } catch (Exception ex) {
        }
        return ListaPais;
    }

    public void setListaPais(List<SelectItem> ListaPais) {
        this.ListaPais = ListaPais;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<SelectItem> getListaTipoInmueble() {
        try {
            ListaTipoInmueble = new ArrayList();
            InterfaceTipoInmueble daoTipoInmueble = new DaoTipoInmueble();
            tipoInmuebles = daoTipoInmueble.verTodo();
            for (TipoInmueble tipoInmueble1 : tipoInmuebles) {
                tipoInmueble = tipoInmueble1;
                this.ListaTipoInmueble.add(new SelectItem(tipoInmueble.getIdTipoInmueble().toString(), tipoInmueble.getNomTipoInmueble()));
            }
        } catch (Exception ex) {
        }
        return ListaTipoInmueble;
    }

    public void setListaTipoInmueble(List<SelectItem> ListaTipoInmueble) {
        this.ListaTipoInmueble = ListaTipoInmueble;
    }

    public TipoInmueble getTipoInmueble() {
        return tipoInmueble;
    }

    public void setTipoInmueble(TipoInmueble tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    public List<SelectItem> getListaCiudad() {
        return ListaCiudad;
    }

    public void setListaCiudad(List<SelectItem> ListaCiudad) {
        this.ListaCiudad = ListaCiudad;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public ClasePregunta getClasePregunta() {
        return clasePregunta;
    }

    public void setClasePregunta(ClasePregunta clasePregunta) {
        this.clasePregunta = clasePregunta;
    }

    public List<SelectItem> getListaClasePregunta() {
        return ListaClasePregunta;
    }

    public void setListaClasePregunta(List<SelectItem> ListaClasePregunta) {
        this.ListaClasePregunta = ListaClasePregunta;
    }

    public SubclasePregunga getSubclasePregunga() {
        return subclasePregunga;
    }

    public void setSubclasePregunga(SubclasePregunga subclasePregunga) {
        this.subclasePregunga = subclasePregunga;
    }

    public List<SelectItem> getListaSubClasePregunta() {
        return listaSubClasePregunta;
    }

    public void setListaSubClasePregunta(List<SelectItem> listaSubClasePregunta) {
        this.listaSubClasePregunta = listaSubClasePregunta;
    }

    public List<TipoPregunta> getTipoPreguntas() {
        return tipoPreguntas;
    }

    public void setTipoPreguntas(List<TipoPregunta> tipoPreguntas) {
        this.tipoPreguntas = tipoPreguntas;
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }

    public List<TipoInmueble> getTipoInmuebles() {
        return tipoInmuebles;
    }

    public void setTipoInmuebles(List<TipoInmueble> tipoInmuebles) {
        this.tipoInmuebles = tipoInmuebles;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(List<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public List<ClasePregunta> getClasePreguntas() {
        return clasePreguntas;
    }

    public void setClasePreguntas(List<ClasePregunta> clasePreguntas) {
        this.clasePreguntas = clasePreguntas;
    }

    public List<SubclasePregunga> getSubclasePregungas() {
        return subclasePregungas;
    }

    public void setSubclasePregungas(List<SubclasePregunga> subclasePregungas) {
        this.subclasePregungas = subclasePregungas;
    }

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public int getResFinal() {
        return resFinal;
    }

    public void setResFinal(int resFinal) {
        this.resFinal = resFinal;
    }

    public List<SelectItem> getListaLocalidad() {
        return ListaLocalidad;
    }

    public void setListaLocalidad(List<SelectItem> ListaLocalidad) {
        this.ListaLocalidad = ListaLocalidad;
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public List<SelectItem> getListaEstrato() {
        return ListaEstrato;
    }

    public void setListaEstrato(List<SelectItem> ListaEstrato) {
        this.ListaEstrato = ListaEstrato;
    }

    public Estrato getEstrato() {
        return estrato;
    }

    public void setEstrato(Estrato estrato) {
        this.estrato = estrato;
    }

    public List<Estrato> getEstratos() {
        return estratos;
    }

    public void setEstratos(List<Estrato> estratos) {
        this.estratos = estratos;
    }

    public MbSlogin getMbSlogin() {
        return mbSlogin;
    }

    public void setMbSlogin(MbSlogin mbSlogin) {
        this.mbSlogin = mbSlogin;
    }

    public String getFinalidadAvaluo() {
        return finalidadAvaluo;
    }

    public void setFinalidadAvaluo(String finalidadAvaluo) {
        this.finalidadAvaluo = finalidadAvaluo;
    }

    public Integer getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(Integer metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    public Integer getValorAvaluo() {
        return valorAvaluo;
    }

    public void setValorAvaluo(Integer valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Calendar getFecha1() {
        return fecha1;
    }

    public void setFecha1(Calendar fecha1) {
        this.fecha1 = fecha1;
    }

    public int getAno1() {
        return ano1;
    }

    public void setAno1(int ano1) {
        this.ano1 = ano1;
    }

    public int getMes1() {
        return mes1;
    }

    public void setMes1(int mes1) {
        this.mes1 = mes1;
    }

    public int getDia1() {
        return dia1;
    }

    public void setDia1(int dia1) {
        this.dia1 = dia1;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public Integer getPisoHabitacion() {
        return pisoHabitacion;
    }

    public void setPisoHabitacion(Integer pisoHabitacion) {
        this.pisoHabitacion = pisoHabitacion;
    }

    public Integer getParedHabitacion() {
        return paredHabitacion;
    }

    public void setParedHabitacion(Integer paredHabitacion) {
        this.paredHabitacion = paredHabitacion;
    }

    public Integer getPisoBano() {
        return pisoBano;
    }

    public void setPisoBano(Integer pisoBano) {
        this.pisoBano = pisoBano;
    }

    public Integer getParedBano() {
        return paredBano;
    }

    public void setParedBano(Integer paredBano) {
        this.paredBano = paredBano;
    }

    public Integer getTipoInstalacion() {
        return tipoInstalacion;
    }

    public void setTipoInstalacion(Integer tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

    public Integer getTipoApartamento() {
        return tipoApartamento;
    }

    public void setTipoApartamento(Integer tipoApartamento) {
        this.tipoApartamento = tipoApartamento;
    }

    public boolean isJacuzzi() {
        return jacuzzi;
    }

    public void setJacuzzi(boolean jacuzzi) {
        this.jacuzzi = jacuzzi;
    }

    public boolean isSauna() {
        return sauna;
    }

    public void setSauna(boolean sauna) {
        this.sauna = sauna;
    }

    public boolean isTurco() {
        return turco;
    }

    public void setTurco(boolean turco) {
        this.turco = turco;
    }

    public boolean isMasGarajes() {
        return masGarajes;
    }

    public void setMasGarajes(boolean masGarajes) {
        this.masGarajes = masGarajes;
    }

    public boolean isCitofono() {
        return citofono;
    }

    public void setCitofono(boolean citofono) {
        this.citofono = citofono;
    }

    public boolean isGimnasio() {
        return gimnasio;
    }

    public void setGimnasio(boolean gimnasio) {
        this.gimnasio = gimnasio;
    }

    public boolean isShut() {
        return shut;
    }

    public void setShut(boolean shut) {
        this.shut = shut;
    }

    public boolean isZonasVerdes() {
        return zonasVerdes;
    }

    public void setZonasVerdes(boolean zonasVerdes) {
        this.zonasVerdes = zonasVerdes;
    }

    public boolean isCanchas() {
        return canchas;
    }

    public void setCanchas(boolean canchas) {
        this.canchas = canchas;
    }

    public boolean isOficinaNegocio() {
        return oficinaNegocio;
    }

    public void setOficinaNegocio(boolean oficinaNegocio) {
        this.oficinaNegocio = oficinaNegocio;
    }

    public boolean isTerrazaComunal() {
        return terrazaComunal;
    }

    public void setTerrazaComunal(boolean terrazaComunal) {
        this.terrazaComunal = terrazaComunal;
    }

    public boolean isCircuitoTv() {
        return circuitoTv;
    }

    public void setCircuitoTv(boolean circuitoTv) {
        this.circuitoTv = circuitoTv;
    }

    public boolean isSalaInternet() {
        return salaInternet;
    }

    public void setSalaInternet(boolean salaInternet) {
        this.salaInternet = salaInternet;
    }

    public boolean isVigilancia() {
        return vigilancia;
    }

    public void setVigilancia(boolean vigilancia) {
        this.vigilancia = vigilancia;
    }

    public boolean isCanchaSquash() {
        return canchaSquash;
    }

    public void setCanchaSquash(boolean canchaSquash) {
        this.canchaSquash = canchaSquash;
    }

    public boolean isCuatoEscoltas() {
        return cuatoEscoltas;
    }

    public void setCuatoEscoltas(boolean cuatoEscoltas) {
        this.cuatoEscoltas = cuatoEscoltas;
    }

    public boolean isJardinInfantil() {
        return jardinInfantil;
    }

    public void setJardinInfantil(boolean jardinInfantil) {
        this.jardinInfantil = jardinInfantil;
    }

    public boolean isPiscina() {
        return piscina;
    }

    public void setPiscina(boolean piscina) {
        this.piscina = piscina;
    }

    public boolean isVistaPanoramica() {
        return vistaPanoramica;
    }

    public void setVistaPanoramica(boolean vistaPanoramica) {
        this.vistaPanoramica = vistaPanoramica;
    }

    public boolean isCanchaTenis() {
        return canchaTenis;
    }

    public void setCanchaTenis(boolean canchaTenis) {
        this.canchaTenis = canchaTenis;
    }

    public boolean isConjuntoCerrado() {
        return conjuntoCerrado;
    }

    public void setConjuntoCerrado(boolean conjuntoCerrado) {
        this.conjuntoCerrado = conjuntoCerrado;
    }

    public boolean isJaulaGolf() {
        return jaulaGolf;
    }

    public void setJaulaGolf(boolean jaulaGolf) {
        this.jaulaGolf = jaulaGolf;
    }

    public boolean isPlantaElectrica() {
        return plantaElectrica;
    }

    public void setPlantaElectrica(boolean plantaElectrica) {
        this.plantaElectrica = plantaElectrica;
    }

    public boolean isSalonJuego() {
        return SalonJuego;
    }

    public void setSalonJuego(boolean SalonJuego) {
        this.SalonJuego = SalonJuego;
    }

    public boolean isResidencial() {
        return residencial;
    }

    public void setResidencial(boolean residencial) {
        this.residencial = residencial;
    }

    public boolean isComercial() {
        return comercial;
    }

    public void setComercial(boolean comercial) {
        this.comercial = comercial;
    }

    public boolean isCampestre() {
        return campestre;
    }

    public void setCampestre(boolean campestre) {
        this.campestre = campestre;
    }

    public boolean isPrincipal() {
        return principal;
    }

    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    public boolean isParques() {
        return parques;
    }

    public void setParques(boolean parques) {
        this.parques = parques;
    }

    public Integer getAreaGaraje() {
        return areaGaraje;
    }

    public void setAreaGaraje(Integer areaGaraje) {
        this.areaGaraje = areaGaraje;
    }

    public Integer getNumBloque() {
        return numBloque;
    }

    public void setNumBloque(Integer numBloque) {
        this.numBloque = numBloque;
    }

    public Integer getNumVentanas() {
        return numVentanas;
    }

    public void setNumVentanas(Integer numVentanas) {
        this.numVentanas = numVentanas;
    }

    public Integer getNumApto() {
        return numApto;
    }

    public void setNumApto(Integer numApto) {
        this.numApto = numApto;
    }

    public boolean isIndustrial() {
        return industrial;
    }

    public void setIndustrial(boolean industrial) {
        this.industrial = industrial;
    }

    public boolean isCubierto() {
        return cubierto;
    }

    public void setCubierto(boolean cubierto) {
        this.cubierto = cubierto;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getNomConjunto() {
        return nomConjunto;
    }

    public void setNomConjunto(String nomConjunto) {
        this.nomConjunto = nomConjunto;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHoraA() {
        return horaA;
    }

    public void setHoraA(int horaA) {
        this.horaA = horaA;
    }

    public int getHora2() {
        return hora2;
    }

    public void setHora2(int hora2) {
        this.hora2 = hora2;
    }

    public int getMinA() {
        return minA;
    }

    public void setMinA(int minA) {
        this.minA = minA;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Pago getPago() {
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    public Usuario getUsuario() throws Exception {
        InterfaceUsuario daoUsuario = new DaoUsuario();
        Usuario usuario = daoUsuario.verPorEmail(this.mbSlogin.getEmail());
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
