/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.view;

import com.dao.DaoCiudad;
import com.dao.DaoEstrato;
import com.dao.DaoLocalidad;
import com.dao.DaoPais;
import com.entidad.Ciudad;
import com.entidad.Estrato;
import com.entidad.Localidad;
import com.entidad.Pais;
import com.interfaces.InterfaceCiudad;
import com.interfaces.InterfaceLocalidad;
import com.interfaces.InterfacePais;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Juanda
 */
@ManagedBean
@ViewScoped
public class EstratoMBV implements Serializable {

    private Estrato estrato;
    private List<Estrato> listaEstrato;
    private Localidad localidad;
    private List<Localidad> localidades;
    private List<SelectItem> listaLocalidad;
    private Ciudad ciudad;
    private List<Ciudad> ciudades;
    private List<SelectItem> listaCiudad;
    private Pais pais;
    private List<Pais> paises;
    private List<SelectItem> ListaPais;

    public EstratoMBV() {
        ListaPais = new ArrayList();
        listaCiudad = new ArrayList();
        listaLocalidad = new ArrayList();
        this.pais = new Pais();
        this.ciudad = new Ciudad();
        this.localidad = new Localidad();
        this.estrato = new Estrato(localidad);
    }

    public void cambiarPreguntaCiudad() {
        if (getCiudad().getIdCiudad() == null && getCiudad().getIdCiudad().equals("0")) {
            listaLocalidad = new ArrayList();
        } else {
            listaLocalidad = new ArrayList();
            try {
                InterfaceLocalidad daoLocalidad = new DaoLocalidad();
                localidades = daoLocalidad.verPorCiudad(getCiudad().getIdCiudad());
                for (Localidad localidade : localidades) {
                    localidad = localidade;
                    this.listaLocalidad.add(new SelectItem(localidad.getIdLocalidad().toString(), localidad.getNomLocalidad()));
                }
            } catch (Exception ex) {

            }
        }
    }

    public void cambiarPreguntaPais() {
        if (pais.getIdPais() != null && pais.getIdPais().equals("0")) {
            listaCiudad = new ArrayList();
        } else {
            listaCiudad = new ArrayList();
            try {
                InterfaceCiudad daoCiudad = new DaoCiudad();
                ciudades = daoCiudad.verPorPais(pais.getIdPais());
                for (Ciudad ciudade : ciudades) {
                    ciudad = ciudade;
                    this.listaCiudad.add(new SelectItem(ciudad.getIdCiudad().toString(), ciudad.getNomCiudad()));
                }
            } catch (Exception ex) {

            }

        }
    }

    public void registrar() throws Exception {
        try {

            DaoEstrato daoEstrato = new DaoEstrato();
            daoEstrato.registrar(this.estrato);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO: ", " El registro se realizo correctamente"));

            this.estrato = new Estrato();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public List<Estrato> getListarEstrato() {
        try {
            DaoEstrato daoEstrato = new DaoEstrato();
            this.listaEstrato = daoEstrato.verTodo();
            return this.listaEstrato;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public void actualizar() throws Exception {
        try {
            DaoEstrato daoEstrato = new DaoEstrato();
            daoEstrato.editar(this.estrato);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EXITO:", " Los datos fueron cambiados exitosamente"));

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
        }
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(List<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public List<SelectItem> getListaCiudad() {
        return listaCiudad;
    }

    public void setListaCiudad(List<SelectItem> listaCiudad) {
        this.listaCiudad = listaCiudad;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<SelectItem> getListaPais() {
        try {
            ListaPais = new ArrayList();
            InterfacePais daoPais = new DaoPais();
            paises = daoPais.verTodo();
            for (Pais paise : paises) {
                pais = paise;
                this.ListaPais.add(new SelectItem(pais.getIdPais().toString(), pais.getNomPais()));
            }
        } catch (Exception ex) {
        }
        return ListaPais;
    }

    public void setListaPais(List<SelectItem> ListaPais) {
        this.ListaPais = ListaPais;
    }

    public Estrato getEstrato() {
        return estrato;
    }

    public void setEstrato(Estrato estrato) {
        this.estrato = estrato;
    }

    public List<Estrato> getListaEstrato() {
        return listaEstrato;
    }

    public void setListaEstrato(List<Estrato> listaEstrato) {
        this.listaEstrato = listaEstrato;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public List<SelectItem> getListaLocalidad() {
        return listaLocalidad;
    }

    public void setListaLocalidades(List<SelectItem> listaLocalidad) {
        this.listaLocalidad = listaLocalidad;
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }
}
