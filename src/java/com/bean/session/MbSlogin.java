/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.session;

import clases.Encriptar;
import com.dao.DaoAdmininstrador;
import com.dao.DaoUsuario;
import com.entidad.Admininstrador;
import com.entidad.Usuario;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author AnchorPriceGroup
 */
@ManagedBean
@SessionScoped

public class MbSlogin implements Serializable {

    /**
     * Creates a new instance of MbSlogin
     */
    private String email;
    private String contrasena;
    private Session session;
    private Transaction trans;

    public MbSlogin() {
        HttpSession miSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        miSession.setMaxInactiveInterval(3600);
    }

    public String login() {
        try {
            DaoUsuario daoUsuario = new DaoUsuario();
            DaoAdmininstrador daoAdmininstrador = new DaoAdmininstrador();
            Usuario usuario = daoUsuario.verPorEmail(this.email);

            if (usuario != null) {
                if (usuario.getContrasena().equals(Encriptar.sha512(this.contrasena))) {
                    if (usuario.getTipoPersona().getNombreTipo().equals("Gratuito")) {
                        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                        httpSession.setAttribute("email", this.email);
                        return "/especial/tipoAvaluo";
                    }
                    HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    httpSession.setAttribute("email", this.email);
                    return "/avaluo/avaluofacil";
                }
            }

            Admininstrador admininstrador = daoAdmininstrador.verPorEmail(this.email);

            if (admininstrador != null) {
                if (admininstrador.getContrasena().equals(this.contrasena)) {
                    HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    httpSession.setAttribute("email", this.email);
                    return "/adminin/usuarios";
                }
            }

            this.email = null;
            this.contrasena = null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR DE LOGUEO: ", " El usuario Y/O contraseña son incorrectos "));
            return "/index";

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR FATAL: ", " Por favor contacte con su admininstrador " + ex.getMessage()));
            return null;
        }
    }

    public String cerrarSesion() {
        this.email = null;
        this.contrasena = null;
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        httpSession.invalidate();
        return "/index";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
