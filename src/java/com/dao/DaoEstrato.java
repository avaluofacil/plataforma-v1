/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.Estrato;
import com.interfaces.InterfaceEstrato;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoEstrato implements InterfaceEstrato {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean editar(Estrato idEstrato) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(idEstrato);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean registrar(Estrato idEstrato) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(idEstrato);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Estrato> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Estrato ORDER BY nomEstrato ASC";
            Query query = sesion.createQuery(hql);
            List<Estrato> listaEstrato = query.list();
            return listaEstrato;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Estrato verPorEstrato(int idEstrato) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Estrato Where idEstrato=:idEstrato ORDER BY nomEstrato ASC";
            Query query = sesion.createQuery(hql);
            query.setParameter("idEstrato", idEstrato);
            Estrato estrato = (Estrato) query.uniqueResult();
            return estrato;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Estrato> verPorPais(int idLocalidad) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Estrato est Where est.localidad.idLocalidad=:idLocalidad ORDER BY nomEstrato ASC";
            Query query = sesion.createQuery(hql);
            query.setParameter("idLocalidad", idLocalidad);
            List<Estrato> estratoLista = query.list();
            return estratoLista;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
