/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.Usuario;
import com.interfaces.InterfaceUsuario;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoUsuario implements InterfaceUsuario {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean editar(Usuario usser) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(usser);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean registrar(Usuario usser) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(usser);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Usuario> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Usuario";
            Query query = sesion.createQuery(hql);
            List<Usuario> listaUsuario = query.list();
            return listaUsuario;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Usuario verPorEmail(String email) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Usuario where email=:email";
            Query query = sesion.createQuery(hql);
            query.setParameter("email", email);
            Usuario usuario = (Usuario) query.uniqueResult();
            return usuario;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Usuario verPorEmailDiferente(Integer idUsuario, String email) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Usuario where email=:email and id!=:id";
            Query query = sesion.createQuery(hql);
            query.setParameter("id", idUsuario);
            query.setParameter("email", email);
            Usuario usuario = (Usuario) query.uniqueResult();
            return usuario;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}