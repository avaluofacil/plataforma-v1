/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.Localidad;
import com.interfaces.InterfaceLocalidad;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoLocalidad implements InterfaceLocalidad {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean editar(Localidad localidad) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(localidad);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean registrar(Localidad localidad) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(localidad);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Localidad> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Localidad ORDER BY nomLocalidad ASC";
            Query query = sesion.createQuery(hql);
            List<Localidad> listaLocalidad = query.list();
            return listaLocalidad;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Localidad> verPorCiudad(int idCiudad) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Localidad loc Where loc.ciudad.idCiudad=:idCiudad ORDER BY nomLocalidad ASC";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            List<Localidad> listaLocalidadCiudad = query.list();
            return listaLocalidadCiudad;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Localidad verPorLocalidad(int idLocalidad) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Localidad Where idLocalidad=:idLocalidad ORDER BY nomLocalidad ASC";
            Query query = sesion.createQuery(hql);
            query.setParameter("idLocalidad", idLocalidad);
            Localidad Localidad = (Localidad) query.uniqueResult();
            return Localidad;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
