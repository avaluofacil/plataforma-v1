/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.Admininstrador;
import com.interfaces.InterfaceAdmininstrador;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoAdmininstrador implements InterfaceAdmininstrador {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean editar(Admininstrador idAdmininstrador) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(idAdmininstrador);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean registrar(Admininstrador idAdmininstrador) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(idAdmininstrador);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Admininstrador> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Admininstrador";
            Query query = sesion.createQuery(hql);
            List<Admininstrador> listaAdmininstrador = query.list();
            return listaAdmininstrador;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Admininstrador verPorEmailDiferente(Integer idAdmininstrador, String email) throws Exception {
        sesion = null;
        tx = null;        
        try {
            iniciaOperacion();
            String hql = "from Admininstrador where email=:email and idAdmininstrador!=:idAdmininstrador";
            Query query = sesion.createQuery(hql);
            query.setParameter("idAdmininstrador", idAdmininstrador);
            query.setParameter("correoElectronico", email);
            Admininstrador admininstrador = (Admininstrador) query.uniqueResult();
            return admininstrador;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Admininstrador verPorEmail(String email) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Admininstrador where email=:email";
            Query query = sesion.createQuery(hql);
            query.setParameter("email", email);
            Admininstrador admininstrador = (Admininstrador) query.uniqueResult();
            return admininstrador;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
