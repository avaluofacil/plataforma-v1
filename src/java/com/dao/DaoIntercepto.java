/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.Intercepto;
import com.interfaces.InterfaceIntercepto;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoIntercepto implements InterfaceIntercepto {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean editar(Intercepto idIntercepto) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(idIntercepto);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean registrar(Intercepto idIntercepto) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(idIntercepto);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Intercepto> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Intercepto";
            Query query = sesion.createQuery(hql);
            List<Intercepto> listaIntercepto = query.list();
            return listaIntercepto;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Intercepto verPorIntercepto(int idPais, int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Intercepto int Where int.pais.idPais=:idPais and int.ciudad.idCiudad=:idCiudad and int.localidad.idLocalidad=:idLocalidad and int.estrato.idEstrato=:idEstrato and int.tipoInmueble.idTipoInmueble=:idTipoInmueble";
            Query query = sesion.createQuery(hql);
            query.setParameter("idPais", idPais);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            Intercepto intercepto = (Intercepto) query.uniqueResult();
            return intercepto;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
