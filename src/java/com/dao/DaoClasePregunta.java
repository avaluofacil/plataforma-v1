/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.ClasePregunta;
import com.interfaces.InterfaceClasePregunta;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoClasePregunta implements InterfaceClasePregunta {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean registrar(ClasePregunta clasePregunta) throws Exception {
                sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(clasePregunta);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<ClasePregunta> verTodo() throws Exception {
                sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from ClasePregunta";
            Query query = sesion.createQuery(hql);
            List<ClasePregunta> listaClasePregunta = query.list();
            return listaClasePregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean editar(ClasePregunta clasePregunta) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
