/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.SubclasePregunga;
import com.interfaces.InterfaceSubclasePregunta;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoSubclasePregunta implements InterfaceSubclasePregunta {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean registrar(SubclasePregunga subclasePregunga) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(subclasePregunga);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<SubclasePregunga> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from SubclasePregunga";
            Query query = sesion.createQuery(hql);
            List<SubclasePregunga> listaSubclasePregunta = query.list();
            return listaSubclasePregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean editar(SubclasePregunga tipoDocumento) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SubclasePregunga> verPorClasePregunta(int idClasePregunta) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from SubclasePregunga sub Where sub.clasePregunta.idClasePregunta=:idClasePregunta";
            Query query = sesion.createQuery(hql);
            query.setParameter("idClasePregunta", idClasePregunta);
            List<SubclasePregunga> listaSubclasePregunta = query.list();
            return listaSubclasePregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }
}
