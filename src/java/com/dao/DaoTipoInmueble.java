/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.TipoInmueble;
import com.interfaces.InterfaceTipoInmueble;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoTipoInmueble implements InterfaceTipoInmueble {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean editar(TipoInmueble idInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(idInmueble);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean registrar(TipoInmueble idInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(idInmueble);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<TipoInmueble> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from TipoInmueble";
            Query query = sesion.createQuery(hql);
            List<TipoInmueble> listaTipoInmueble = query.list();
            return listaTipoInmueble;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public TipoInmueble verPorTipoInmueble(int idTipoInmueble) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from TipoInmueble Where idTipoInmueble=:idTipoInmueble";
            Query query = sesion.createQuery(hql);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            TipoInmueble tipoInmueble = (TipoInmueble) query.uniqueResult();
            return tipoInmueble;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
