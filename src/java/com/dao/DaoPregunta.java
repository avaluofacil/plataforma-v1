/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.Pregunta;
import com.interfaces.InterfacePregunta;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoPregunta implements InterfacePregunta {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean registrar(Pregunta idPregunta) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(idPregunta);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Pregunta> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta";
            Query query = sesion.createQuery(hql);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean editar(Pregunta usser) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(usser);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

    @Override
    public List<Pregunta> verPorHabitacione(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta p where p.ciudad.idCiudad=:idCiudad and p.localidad.idLocalidad=:idLocalidad and p.estrato.idEstrato=:idEstrato and p.tipoInmueble.idTipoInmueble=:idTipoInmueble and p.clasePregunta.idClasePregunta='1' and p.subclasePregunga.idSubclasePregunta='1'";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Pregunta> verPorBano(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta p where p.ciudad.idCiudad=:idCiudad and p.localidad.idLocalidad=:idLocalidad and p.estrato.idEstrato=:idEstrato and p.tipoInmueble.idTipoInmueble=:idTipoInmueble and p.clasePregunta.idClasePregunta='1' and p.subclasePregunga.idSubclasePregunta='2'";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Pregunta> verPorGaraje(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta p where p.ciudad.idCiudad=:idCiudad and p.localidad.idLocalidad=:idLocalidad and p.estrato.idEstrato=:idEstrato and p.tipoInmueble.idTipoInmueble=:idTipoInmueble and p.clasePregunta.idClasePregunta='1' and p.subclasePregunga.idSubclasePregunta='3'";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Pregunta> verPorOtra(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta p where p.ciudad.idCiudad=:idCiudad and p.localidad.idLocalidad=:idLocalidad and p.estrato.idEstrato=:idEstrato and p.tipoInmueble.idTipoInmueble=:idTipoInmueble and p.clasePregunta.idClasePregunta='1' and p.subclasePregunga.idSubclasePregunta='4'";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Pregunta> verPorAdicional(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta p where p.ciudad.idCiudad=:idCiudad and p.localidad.idLocalidad=:idLocalidad and p.estrato.idEstrato=:idEstrato and p.tipoInmueble.idTipoInmueble=:idTipoInmueble and p.clasePregunta.idClasePregunta='2'";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Pregunta> verPorGeografica(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta p where p.ciudad.idCiudad=:idCiudad and p.localidad.idLocalidad=:idLocalidad and p.estrato.idEstrato=:idEstrato and p.tipoInmueble.idTipoInmueble=:idTipoInmueble and p.clasePregunta.idClasePregunta='3'";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Pregunta> verFiltro(int idCiudad, int idLocalidad, int idEstrato, int idTipoInmueble) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Pregunta p where p.ciudad.idCiudad=:idCiudad and p.localidad.idLocalidad=:idLocalidad and p.estrato.idEstrato=:idEstrato and p.tipoInmueble.idTipoInmueble=:idTipoInmueble";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            query.setParameter("idLocalidad", idLocalidad);
            query.setParameter("idEstrato", idEstrato);
            query.setParameter("idTipoInmueble", idTipoInmueble);
            List<Pregunta> listaPregunta = query.list();
            return listaPregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

}
