/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.TipoDocumento;
import com.interfaces.InterfaceTipoDocumento;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoTipoDocumento implements InterfaceTipoDocumento {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean registrar(TipoDocumento tipoDocumento) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(tipoDocumento);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<TipoDocumento> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from TipoDocumento";
            Query query = sesion.createQuery(hql);
            List<TipoDocumento> listaTipoDocumento = query.list();
            return listaTipoDocumento;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public TipoDocumento verPorTipoDocumento(Integer idTipoDocumento) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from TipoDocumento where id=:id";
            Query query = sesion.createQuery(hql);
            query.setParameter("id", idTipoDocumento);
            TipoDocumento tipoDocumento = (TipoDocumento) query.uniqueResult();
            return tipoDocumento;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean editar(TipoDocumento tipoDocumento) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
