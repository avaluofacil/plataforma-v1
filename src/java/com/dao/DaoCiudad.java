/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entidad.Ciudad;
import com.interfaces.InterfaceCiudad;
import com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Juanda
 */
public class DaoCiudad implements InterfaceCiudad {

    private Session sesion;
    private Transaction tx;

    @Override
    public boolean editar(Ciudad idCiudad) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.update(idCiudad);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public boolean registrar(Ciudad idCiudad) throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            sesion.save(idCiudad);
            tx.commit();
            return true;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Ciudad> verTodo() throws Exception {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Ciudad ORDER BY nomCiudad ASC";
            Query query = sesion.createQuery(hql);
            List<Ciudad> listaCiudad = query.list();
            return listaCiudad;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public List<Ciudad> verPorPais(int idPais) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Ciudad ciu Where ciu.pais.idPais=:idPais ORDER BY nomCiudad ASC";
            Query query = sesion.createQuery(hql);
            query.setParameter("idPais", idPais);
            List<Ciudad> listaSubclasePregunta = query.list();
            return listaSubclasePregunta;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Ciudad verPorCiudad(int idCiudad) {
        sesion = null;
        tx = null;
        try {
            iniciaOperacion();
            String hql = "from Ciudad Where idCiudad=:idCiudad ORDER BY nomCiudad ASC";
            Query query = sesion.createQuery(hql);
            query.setParameter("idCiudad", idCiudad);
            Ciudad ciudad = (Ciudad) query.uniqueResult();
            return ciudad;
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

}
