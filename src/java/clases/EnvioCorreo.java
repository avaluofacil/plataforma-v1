/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.faces.context.FacesContext;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;

/**
 *
 * @author Anchor Price Group
 */
public class EnvioCorreo {

    public static void correoElectronico(String correo, String nombre, String nombreArchivo) throws MessagingException {
        Properties props = new Properties();
        props.put("mail.smtp.host", "mail.anchorpricegroup.com");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.port", "25");
        props.setProperty("mail.smtp.user", "avaluofacil@anchorpricegroup.com");
        props.setProperty("mail.smtp.auth", "true");
        Session session = Session.getDefaultInstance(props, null);
        BodyPart texto = new MimeBodyPart();
        texto.setText("Señor(a) " + nombre + " " + correo + "\r\n\r\n\r\nAgradecemos su confianza al usar la primera aplicación en el mundo que realiza y emite avaluos de inmuebles online." + "\r\n\r\n\r\natt:\r\n\r\n" + "Equipo innovador de Avaluofacil.com");
        BodyPart adjunto = new MimeBodyPart();
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String carpetaAvaluo = (String) servletContext.getRealPath("/Avaluos");
        adjunto.setDataHandler(new DataHandler(new FileDataSource(carpetaAvaluo + "/" + nombreArchivo + ".pdf")));
        adjunto.setFileName(nombreArchivo + ".pdf");
        MimeMultipart multiParte = new MimeMultipart();
        multiParte.addBodyPart(texto);
        multiParte.addBodyPart(adjunto);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("avaluofacil@anchorpricegroup.com"));
        message.addRecipient(
                Message.RecipientType.TO,
                new InternetAddress("titanecol@gmail.com"));
        message.addRecipient(
                Message.RecipientType.BCC,
                new InternetAddress("avaluofacil@gmail.com"));
        message.setSubject("Avaluo de Inmueble");
        message.setContent(multiParte);

        Transport t = session.getTransport("smtp");
        t.connect("avaluofacil@anchorpricegroup.com", "Le0nar201");
        t.sendMessage(message, message.getAllRecipients());
        t.close();
    }
}