/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.Serializable;

/**
 *
 * @author Anchor Price Group
 */

public class Pago implements Serializable{

    public String apiKey;
    public String merchantId;
    public String message;
    public String referenceCode;
    public String txValue;
    public String newValue;
    public String currency;
    public String transactionState;
    public String firmaCadena;
    public String firmaCreada;
    public String firma;
    public String referencePol;
    public String cus;
    public String extra1;
    public String pseBank;
    public String lapPaymentMethod;
    public String transactionId;
    public Integer valorAvaluo;
    public String buyerMailer;
    public String polResponseCode;
    public String polTransactionState;

    public Pago() {
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getTxValue() {
        return txValue;
    }

    public void setTxValue(String txValue) {
        this.txValue = txValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(String transactionState) {
        this.transactionState = transactionState;
    }

    public String getFirmaCadena() {
        return firmaCadena;
    }

    public void setFirmaCadena(String firmaCadena) {
        this.firmaCadena = firmaCadena;
    }

    public String getFirmaCreada() {
        return firmaCreada;
    }

    public void setFirmaCreada(String firmaCreada) {
        this.firmaCreada = firmaCreada;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getReferencePol() {
        return referencePol;
    }

    public void setReferencePol(String referencePol) {
        this.referencePol = referencePol;
    }

    public String getCus() {
        return cus;
    }

    public void setCus(String cus) {
        this.cus = cus;
    }

    public String getExtra1() {
        return extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getPseBank() {
        return pseBank;
    }

    public void setPseBank(String pseBank) {
        this.pseBank = pseBank;
    }

    public String getLapPaymentMethod() {
        return lapPaymentMethod;
    }

    public void setLapPaymentMethod(String lapPaymentMethod) {
        this.lapPaymentMethod = lapPaymentMethod;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getValorAvaluo() {
        return valorAvaluo;
    }

    public void setValorAvaluo(Integer valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    public String getBuyerMailer() {
        return buyerMailer;
    }

    public void setBuyerMailer(String buyerMailer) {
        this.buyerMailer = buyerMailer;
    }

    public String getPolResponseCode() {
        return polResponseCode;
    }

    public void setPolResponseCode(String polResponseCode) {
        this.polResponseCode = polResponseCode;
    }

    public String getPolTransactionState() {
        return polTransactionState;
    }

    public void setPolTransactionState(String polTransactionState) {
        this.polTransactionState = polTransactionState;
    }
}
